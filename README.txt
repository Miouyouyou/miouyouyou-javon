!!THIS IS A DRAFT. HUGE MODIFICATIONS ARE STILL POSSIBLE.!!

Javon is a library intended to execute Java instructions represented by
specific JSON structures.
The main use of this library is to couple it with a HTTP Server to
execute Java instructions remotely.

Dependencies :
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻

- com.miouyouyou.libraries : miouyouyou-java-helpers
- com.miouyouyou.libraries : miouyouyou-json-helpers
- net.sf.trove4j : trove4j
- org.json : json 

How to use this :
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻

The main methods useful for external applications are :

JSONParsers.parse_external_argument(String, ObjectStorage)
ResultDisplay.javon(Object)
ResultDisplay.javon_reference_object(Object, Objet)
ResultDisplay.has_javon_representation(Object)

Only the first method is useful to parse and execute instructions, represented
by a <Javon-structure>. 
ResultDisplay methods are only useful if you want to represent objects as
<Javon-structure>, or other JSON structures if they cannot be represented as
such, in order to communicate with a JSON client.
See the <Javon-structure> section for informations about JSON structures
parsed as such.

JSONParsers.parse_external_argument(String, ObjectStorage) parses an 
instruction, represented by a <Javon-structure> String, and return the
Object returned by the executed instruction.
The second argument of this method, is an Object implementing 
the com.miouyouyou.libraries.javon.interfaces.ObjectStorage interface. It
will be used to parse references defined in <Javon-structure>. See below for 
more informations about "references" in <Javon-structure>.
If you pass a String that isn't a valid JSON Structure or isn't a 
<Javon-structure> a com.miouyouyou.libraries.javon.exceptions.MalformedCall
exception will be thrown. This exception is a RuntimeException.
Also, if something goes wrong during the execution of the instruction, a 
RuntimeException will be thrown.

ResultDisplay.javon(Object) returns a String, representing a JSON structure.
The JSON Structure can be a <Javon-structure> if the Object can surely be 
represented as such. Else, it will be a JSON structure that cannot be parsed
as an instruction.
These specific JSON structures, returned by ResultDisplay.javon, that are not
<Javon-structure> are :
* Exceptions (returned if Object is an Exception instance) :
{"category":"Exception", "getClass":"<Object-class-literal>", 
 "cause":"<Exception-cause>", "message":"<Exception-message>", 
 "stacktrace":"<Displayed-stacktrace>", "toString":"<Exception#toString()>"}
* Other objects :
{"category":"Unknown", "getClass":"<Object-class-literal>", 
 "toString":"<Object#toString()>"}
Where :
- <Object-class-literal> is the literal name of the Object's class. Examples :
java.lang.RuntimeException, java.lang.Object[].
- <Exception-cause> is the cause of the exception provided by 
  Exception#getCause()
- <Exception-message> is the message provided by Exception#getMessage()
- <Displayed-stacktrace> is the message provided by 
  Exception#printStackTrace(Writer)

To know if ResultDisplay.javon(Object) will return a <Javon-structure> or
not, use ResultDisplay.has_javon_representation(Object) which will return a
boolean indicating if the Object can surely be represented as a 
<Javon-structure> or not.

ResultDisplay.javon_reference_object(Object, Object) return a <Javon-structure>
defining a "reference". 
If you pass this reference String directly, or embed it appropriately in another
<Javon-structure>, JSONParsers.parse_external_argument will try to extract the
object referenced from the ObjectStorage provided, and use it in the provided 
instruction.

Note that :
* saving data to the ObjectStorage is up to the external application;
* JSONParsers.parse_external_argument will ignore any JSON property that is not
used in a <Javon-structure>.

For example, the following JSON strings :
{"category": "Boolean", "value":true}
{"category": "Boolean", "value":true, "ID":75}
{"category": "Boolean", "value":true, "X-Super-Cow":"Moooh"}
will all return (Boolean) true.

You can use this property to implement the saving mechanism.

Here is a simple example, using NanoHTTPD.
NanoHTTPD can be downloaded here :
https://raw.githubusercontent.com/NanoHttpd/nanohttpd/master/core/src/main/java/fi/iki/elonen/NanoHTTPD.java
See http://nanohttpd.com for more informations about NanoHTTPD

--8<---------------------------------------------------------------------------
import com.miouyouyou.libraries.javon.JSONParsers;
import com.miouyouyou.libraries.javon.ResultDisplay;
import com.miouyouyou.libraries.javon.ObjectLibrary;
import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;

// Needed to treat HTTP request data with NanoHTTPD
import java.util.HashMap;

/* Comments concerning Javon are written like this */
// Comments concerning NanoHTTPD are written like this

public class MyApplication extends NanoHTTPD {

  /* ObjectLibrary is an implementation of ObjectStorage provided 
     with the library */
  ObjectStorage reference_storage = new ObjectLibrary();

  // Set the port bound by NanoHTTPD. Here 8080.
  public MyApplication() {
    super(8080);
  }
  
  // This is where NanoHTTPD treat HTTP Requests
  @Override
  public Response serve(IHTTPSession http_session) {

    Method http_method = http_session.getMethod();
    // We only treat POST requests
    if (http_method == Method.POST) {
      HashMap<String,String> post_informations = new HashMap<String,String>();
      try {
        // Copy the POST request informations to the HashMap
        http_session.parseBody(post_informations);
        // Return an error if no data were provided in the post request
        if (!post_informations.containsKey("postData")) {
          return new NanoHTTPD.Response("{\"message\":\"No data provided\"}");
        }

        /* Parse the String provided as Data during the POST Request.
           If the String does not represent a <Javon-structure>, a 
           RuntimeException will be thrown here, then catched below. */
        String post_data = post_informations.get("postData");
        Object instruction_result = 
          JSONParsers.parse_external_argument(post_data, reference_storage);

        /* We save any Object that cannot be represented. This is quite naive
           but it shows how you can implemenent saving objects to the 
           ObjectStorage and provide a reference. */
        String jsoned_result;
        if (ResultDisplay.has_javon_representation(instruction_result)) {
          jsoned_result = ResultDisplay.javon(instruction_result);
        } 
        else {
          Object reference_id = reference_storage.store(instruction_result);
          jsoned_result = 
            ResultDisplay.javon_reference_object(reference_id, 
                                                 instruction_result);
        }
        
        // Display the result back to the client
        return new NanoHTTPD.Response(jsoned_result);
      }
      catch (Exception e) {
        String result_display_exception = ResultDisplay.javon(e);
        return new NanoHTTPD.Response(result_display_exception); 
      }
    }
    else {
      return new NanoHTTPD.Response("{\"message\":\"Use a POST request !\"}");
    }
      
  }

  public static void main(String[] args) throws java.io.IOException {
    MyApplication application = new MyApplication();
    application.start();
    // NanoHTTPD terminate as soon as the method terminates. This code ensures
    // that the method will only terminate when the program is terminated.
    // Feel free to implement other termination mechanisms
    while(true) {
      System.in.read();
    }
  }

}

---------------------------------------------------------------------------->8--

To compile this example, you'll need, at least :
- NanoHTTPD (NanoHTTPD.java)
- miouyouyou-java-helpers (miouyouyou-java-helpers-20141118.jar)
- miouyouyou-json-helpers (miouyouyou-json-helpers-20141129.jar)
- trove4j (trove4j-3.0.3.jar)
- org.json (json-20080701.jar)
- miouyouyou-javon (miouyouyou-javon-20141118.jar)

Also, make sure that the port bound by HTTPD is not publicly accessible !
Coupling this library with a publicly accessible server is discouraged. See
below for more informations.

When you've got everything ready, execute :
$ javac path/to/NanoHTTPD.java MyApplication.java -cp dependencies/jar/folder/*:.
Then to run the application :
$ java -cp path/to/jar/files/*:. MyApplication
Then use an HTTP client able to send POST request, and use it to send POST
requests containing <Javon-structure> as data to localhost:8080

Differences with RPC libraries and security implications :
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻

The main difference between this library, which is an arbitrary instruction 
execution library, and remote procedure call libraries, is what can be executed.

This library will execute ANY instruction than can be represented as a
<Javon-structure>.

In comparaison, most remote procedure call libraries require developpers using
them in their application, to define the procedures (methods essentially) that
can be called explicitly. Reversely, any method that is not allowed explicitly 
cannot be executed through those libraries.

Javon is meant to be used internally, in order to execute Java instructions,
remotely, with any other programming language that can output JSON structures.
It is intended that as long as the instructions are valid, this library will
execute them. For example, when it comes to calling a method, the only things
you have to know are :
- The full class name of the class receiving the call, OR the instantiated 
  object receiving it (from the documentation of the class implementing such 
  methods)
- The signature of the method (from the same documentation)
- The arguments to pass (from the same documentation)
- How to represent this method call and the required objects in Javon 
  (from this documentation)

The main security recommandation is, unless you clearly understand the security
implications behind it : 
Do NOT couple Javon with an unprotected public server ! This will be equivalent
to creating remote Java code execution service on your machine !
If you only need to execute some specific Java methods remotely, use a RPC 
library instead ! This is the main purpose of a Remote Procedure Call library,
and the reason why called 'procedures' need to be defined explicitly.

So, in short, do NOT couple Javon with an unprotected public server !

Keys used in JSON structures by Javon :
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻

The values associated to the following keywords might be used during the
execution of a Javon instruction.
You can consider them as 'reserved keywords', but depending on the instruction,
the presence of such keywords might be ignored :

arguments       call             category
name            options (¹)      receiver
signature       type             value

¹ Currently unused

<Javon-structure>
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻

Currently, the following JSON structures, referenced as <Javon-structure>,
are understood :

{"category": "Boolean", "value": <boolean-value>}
{"category": "Number", "type":"<primitive-number-classname>", "value": <number>}
{"category": "Object", "type":"null"}
{"category": "Object", "type":"Character", "value":<character>}
{"category": "Object", "type":"String", "value":"<string>"}
{"category": "Object", "type":"Class", "value":"<classname-literal>"}
{"category": "Object", "type":"Reference", "value":<ObjectStorage-reference>}
{"category": "Array", "type":"<classname-literal>", "value":[<comma-separated-json-values-or-Javon-structures>]}
{"call":"new", "receiver":"<classname-literal>"}
{"call":"new", "receiver":"<classname-literal>", "signature":[<comma-separated-classname-literals>], "arguments":[<comma-separated-json-values-or-Javon-structures>]}
{"call":"invoke_method", "receiver":<Javon-structure>}
{"call":"invoke_method", "receiver":<Javon-structure>, "name":"<method-name>", "signature":[<comma-separated-classname-literals>], "arguments":[<comma-separated-json-values-or-Javon-structures>]}

With :
- <boolean-value> being true or false, with or without quotes. 
  (Default : without)
- <classname-literal> being the full literal name of a type or class, 
  between quotes.
  Examples :
  "short", "java.lang.String", "double[]", "java.lang.Character[][]"
- <primitive-number-classname> being a <classname-literal> representing a
  primitive type (e.g. : "byte") or a boxed primitive class.
  (e.g. : java.lang.Byte)
- <number> being any decimal number without quotes OR a <character> OR one of
  the following special floating-point value name, between quotes : "NaN", 
  "Infinity", "-Infinity". See below for examples. The numbers will always be
  casted to the defined type. "e" or "E" is allowed at the end of floating-point
  values. Example : 157.10e25
- <character> being any quoted character (e.g. : "a") or an unsigned 16 bits 
  integer representing an UTF-8 codepoint. (e.g. : 7533 for ᵭ).
  Since the main purpose of this structure is to output a 'char' value, the
  same limitations of that type applies here. Characters needing more than one
  codepoint cannot be represented with this structure.
- <string> being any valid character string. Be careful, by default, JSON
  parsers do not parse multiline character strings. The JSON parser used in this
  project is no exception, so multiline strings need to be formatted appro-
  priately. Same goes for control characters, which must be escaped properly.
- <ObjectStorage-reference> is a reference that can be used with the current
  implementation of com.miouyouyou.libraries.javon.interfaces.ObjectStorage. 
  Example :
  If you use the provided implementation "ObjectLibrary", only long values will
  be accepted as <ObjectStorage-reference> since ObjectLibrary associate stored
  objects to long values. With a local implementation of ObjectStorage, others
  values might be accepted.
- <json-values> represent any value that can be directly represented in JSON.
  Be careful, though, the parser currently used internally, org.json, parse
  some quoted values as direct values. Values like "true" or "null" will be
  interpreted the same as true or null, even if they are quoted. If you need
  to pass a String representing these values, use a <Javon-structure> instead.
- <comma-separated-json-values-or-Javon-structures> is a list composed of zero
  or more <json-values> and/or <Javon-structure>, separated by commas if there
  are two values or more.
  Examples :
  "a"
  {"category":"Object", "type":"String", value:"true"}, "b"
  468721, {"category":"Number", "type":"long", value:-784561}, 24.10
- <comma-separated-classname-literals> is a list composed of zero or more
  <classname-literal>, separated by commas when there are two values or more.
- <method-name> is a String representing the name of the method to call, without
  parenthesis. Examples : "trim", "put", "hashCode".

Notes :

Concerning "category" : "Object", "type" : "null"
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻
In it's current state, Javon uses the "org.json" JSON Parser. This parser
parse null and "null" values in a very specific way, that requires supplementary
checks in order return a real 'null' value.
If you want to encode a 'null' value, please use the appropriate 
<Javon-structure>.
Currently, both {"category":"Object", "type":"null"} and 
{"category":"Object","type":"<any-type>", "value":null} work. If in doubt, 
please use {"category":"Object", "type":"null"} as it is the most explicit.

Concerning "category" : "Number" and overflowing floating-point values
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻
In it's current state, Javon uses the org.json JSON Parser. This parser will
throw an exception if the floating-point number provided overflows to Infinity
or -Infinity. If you wish to represent "Infinity" values, use the appropriate
<Javon-structure>. If you still wish to provide a floating-point value that might
overflow to infinity, you might quote it before-hand.

Note that quoting integer values might produce NumberFormatException if they
overflow, so *do not quote values* except in the case described in the preceding
paragraph.

Concerning "category" : "Number" and special floating-point values
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻
Since there's a difference between the "float" and "double" representations of :
- "NaN",
- "Infinity", 
- "-Infinity"; 
Float.NaN, Float.POSITIVE_INFINITY and Float.NEGATIVE_INFINITY will be returned
if the type is "float" or "java.lang.Float", otherwise the 'Double' version of
these values will be returned if the type is "double".
If the type is neither "float" or "double", but such a special floating-point 
value is provided, the Double version of the specified special number will be 
casted to the desired type.
Examples :
{"category": "Number", "type":"java.lang.Character", "value": 8364}
-> (Character) '€'
{"category": "Number", "type":"java.lang.Integer", "value": 78453.12} 
-> (Integer) 78453
{"category": "Number", "type":"java.lang.Float", "value": "€"}
-> (Float) 8364.0
{"category": "Number", "type":"float", "value":"NaN"}
-> (Float) Float.NaN
{"category": "Number", "type":"java.lang.Float", "value":"-Infinity"}
-> (Float) Float.NEGATIVE_INFINITY
{"category": "Number", "type":"double", "value":"-Infinity"}
-> (Double) Double.NEGATIVE_INFINITY
{"category": "Number", "type":"int", "value":"Infinity"}
-> (Integer) 2147483647
{"category": "Number", "type":"char", "value":"NaN"}
-> (Character) '\u0000'
{"category": "Number", "type":"short", "value":"Infinity"}
-> (Short) -1

( -> indicates the generated result )

Concerning "category" : "Array" and the values provided
-------------------------------------------------------
Only the component type of the array should be specified in "type", not the
array class literal itself.

Example :
{"category":"Array", "type":"short", "value":[789, 12410, 88]}
-> new short[] {789, 12410, 88}

Primitive types and boxed primitive classes are *NOT* compatible, when they
represent the component type of an Array. You cannot pass a long Array to a
method awaiting a Long array, and vice-versa. Therefore, set the type carefully.

Examples :
{"category":"Array", "type":"long", "value":[16751, 12410316978, -81216811]}
-> new long[] {16751, 12410316978, -81216811}
{"category":"Array", "type":"java.lang.Long", "value":[16751, 12410316978, -81216811]}
-> new Long[] {16751, 12410316978, -81216811}
The two generated Array cannot be interchanged when it comes to method arguments.

Only values that are compatible with the array component type will be extracted
from the JSON Array provided. Numbers are always extrated and casted 
appropriately, though.

Examples :
{"category":"Array", "type":"int", "value":[157.123, "€", {"category":"Number", "type":"double", "value":"NaN"}, "Hello"]}
-> new int[] {157, 0}
{"category":"Array", "type":"long", "value":[9007199254740993, {"category":"Number", "type":"char", "value":"€"}]}
-> new long[] {9007199254740993, 8364}
{"category":"Array", "type":"java.lang.String", "value":["a", {"category":"Number", "type":"char", "value":"b"}, {"category":"Object", "type":"String", "value":"c"}]}
-> new String[] {"a", "c"}
{"category":"Array", "type":"java.lang.CharSequence", "value":["a", 2, {"category":"Object", "type":"String", "value":"c"}]}
-> new CharSequence[] {"a", "c"}

Concerning "category" : "Array" and multidimensional Array
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻
While multidimensional Array can be specified, remember that multidimensional 
Array in Java are Array where each dimension component type is the class of
the lower dimensions Array. That means that only the last dimension of a 
multidimensional Array can store concrete values. All the upper dimensions 
can only store Array typed values. 
For example, in an int[][][] Array :
- the last dimension int[][][*] (marked with a '*' character) can only store 
  integer values;
- the previous dimension, int[][*][] can only store int[] values;
- and the first dimension, int[*][][], can only store int[][] values !
This implies that multidimensional Array are not real nested Array, since the 
number of dimensions is fixed. The size of the dimensions can vary but you 
cannot extend a two-dimensional Array to a three-dimensional Array. For 
example, you cannot map this JSON nested Array : [[[1, 2]],[3]] to any Java 
Array.

So, currently, when dealing with nested Array, Javon will try limit itself to
the lowest number of nested dimensions, then only extract the values of the last
dimensions.

Example :
{"category":"Array", "type":"java.lang.String", "value":[["a", "b"], ["c"], ["d", "e", "f"]]}
-> new String[][] {{"a", "b"}, {"c"}, {"d", "e", "f"}}
{"category":"Array", "type":"java.lang.String", "value":[["a", "b"], [["c", "d"], "e"], "f"]}
-> new String[][] {{"a", "b"} {"e"}}
{"category":"Array", "type":"java.lang.String", "value":["a", ["b"], [["c", "d"], "e"], "f"]}
-> new String[][] {{"b"},{"e"}}
{"category":"Array", "type":"java.lang.Number", "value":[[27.0, 56, {"category": "Object", "type":"null"}], [789, -785156154564, {"category":"Number", "type":"float", "value":"Infinity"}]]}
-> new Number[][] {{27.0D, 56, null}, {789, -785156154564L, Float.POSITIVE_INFINITY}}


Examples for each call type :
⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻

* Boolean

{"category": "Boolean", "value": true} 
-> (Boolean) true
{"category": "Boolean", "value": false} 
-> (Boolean) false
{"category": "Boolean"}
-> null
{"category": "Boolean", "value": "false"}
-> (Boolean) false
{"category": "Boolean", "value": "true"}
-> (Boolean) true

* Number

{"category": "Number", "type":"byte", "value":127}
-> (Byte) 127
{"category": "Number", "type":"byte", "value":140}
-> (Byte) -116
{"category": "Number", "type":"short", "value":24123}
-> (Short) 24123
{"category": "Number", "type":"short", "value":-34000}
-> (Short) 31536
{"category": "Number", "type":"char", "value":"÷"}
-> (Character) '÷'
{"category": "Number", "type":"char", "value":4309}
-> (Character) 'ვ'
{"category": "Number", "type":"char", "value":-12474}
-> (Character) '콆'
{"category": "Number", "type":"int", "value":874919}
-> (Integer) 874919
{"category": "Number", "type":"int", "value":-3478912674}
-> (Integer) 816054622
{"category": "Number", "type":"int", "value":"÷"}
-> (Integer) 247
{"category": "Number", "type":"long", "value":18014398509481989}
-> (Long) 18014398509481989
{"category": "Number", "type":"long", "value":36893488147419103232}
-> (Long) 9223372036854775807
{"category": "Number", "type":"float", "value":7894.6879}
-> (Float) 7894.688
{"category": "Number", "type":"float", "value":"€"}
-> (Float) 8364.0
{"category": "Number", "type":"float", "value":154.25}
-> (Float) 154.25
{"category": "Number", "type":"float", "value":1.5425e2}
-> (Float) 154.25
{"category": "Number", "type":"float", "value":15e1578}
-> throw new org.json.JSONException("JSON does not allow non-finite numbers.")
{"category": "Number", "type":"float", "value":"15e1578"}
-> Float.POSITIVE_INFINITY
{"category": "Number", "type":"double", "value":7894.6879}
-> (Double) 7894.6879
{"category": "Number", "type":"double", "value":15e1578}
-> throw new org.json.JSONException("JSON does not allow non-finite numbers.")
{"category": "Number", "type":"double", "value":"15e1578"}
-> Double.POSITIVE_INFINITY
{"category": "Number", "type":"double", "value":18014398509481989}
-> (Double) 1.8014398509481988E16
{"category": "Number", "type":"java.lang.Character", "value":457.126}
-> (Character) 'ǉ'
{"category": "Number", "type":"java.lang.Integer", "value":"NaN"}
-> (Integer) 0
{"category": "Number", "type":"java.lang.Short", "value":"-Infinity"}
-> (Short) 0
{"category": "Number", "type":"java.lang.Short", "value":"Infinity"}
-> (Short) -1
{"category": "Number", "type":"java.lang.Integer", "value":"-Infinity"}
-> (Integer) -2147483648
{"category": "Number", "type":"java.lang.Integer", "value":"Infinity"}
-> (Integer) 2147483647
{"category": "Number", "type":"java.lang.Short", "value":"NaN"}
-> (Short) 0
{"category": "Number", "type":"java.lang.Number", "value":0}
-> throw new IllegalArgumentException("This class requires a primitive or boxed primitive class as argument...")

* Null

{"category": "Object", "type":"null"}
{"category": "Object", "type":"<any-type>", value:null}

* Character

{"category": "Object", "type":"Character", "value":"鴉"}
-> (Character) '鴉'
{"category": "Object", "type":"Character", "value":7454}
-> (Character) 'ᴞ'

* String

{"category": "Object", "type":"String", "value":"<string>"}
-> "<string>"
{"category": "Object", "type":"String", "value":"\"a\n\""}
-> "\"a\n\""

* Class

{"category": "Object", "type":"Class", "value":"int"}
-> int.class
{"category": "Object", "type":"Class", "value":"java.lang.Integer"}
-> java.lang.Integer.class
{"category": "Object", "type":"Class", "value":"java.lang.String[][][]"}
-> java.lang.String[][][].class
{"category": "Object", "type":"Class", "value":"void"}
-> void.class

* Array 

{"category": "Array", "type":"int", "value":[27, {"category":"Number", "type":"long", "value":4596}, {"category":"Object", "type":"Character", "value":"a"}, {"category":"Object", "type":"null"}]}
-> new int[] {27, 4596, 97}
{"category": "Array", "type":"java.lang.Integer", "value":[27, {"category":"Number", "type":"long", "value":4596}, {"category":"Object", "type":"Character", "value":"a"}, {"category":"Object", "type":"null"}]}
-> new Integer[] {27, 4596, 97, null}
{"category": "Array", "type":"java.lang.Short", "value":[[0, 7], [9, 8]]}
-> new Short[][] {{0, 7}, {9, 8}}
{"category":"Array", "type":"int", "value":[157.123, "€", {"category":"Number", "type":"double", "value":"NaN"}, "Hello"]}
-> new int[] {157, 0}
{"category":"Array", "type":"long", "value":[9007199254740993, {"category":"Number", "type":"char", "value":"€"}]}
-> new long[] {9007199254740993, 8364}
{"category":"Array", "type":"java.lang.String", "value":["a", {"category":"Number", "type":"char", "value":"b"}, {"category":"Object", "type":"String", "value":"c"}]}
-> new String[] {"a", "c"}
{"category":"Array", "type":"java.lang.CharSequence", "value":["a", 2, {"category":"Object", "type":"String", "value":"c"}]}
-> new CharSequence[] {"a", "c"}
{"category":"Array", "type":"java.lang.String", "value":[["a", "b"], ["c"], ["d", "e", "f"]]}
-> new String[][] {{"a", "b"}, {"c"}, {"d", "e", "f"}}
{"category":"Array", "type":"java.lang.String", "value":[["a", "b"], [["c", "d"], "e"], "f"]}
-> new String[][] {{"a", "b"} {"e"}}
{"category":"Array", "type":"java.lang.String", "value":["a", ["b"], [["c", "d"], "e"], "f"]}
-> new String[][] {{"b"},{"e"}}
{"category":"Array", "type":"java.lang.Number", "value":[[27.0, 56, {"category": "Object", "type":"null"}], [789, -785156154564, {"category":"Number", "type":"float", "value":"Infinity"}]]}
-> new Number[][] {{27.0D, 56, null}, {789, -785156154564L, Float.POSITIVE_INFINITY}}

* New Object

{"call":"new", "receiver":"java.lang.Object"}
-> new Object[]
{"call":"new", "receiver":"java.lang.String", "signature":["char[]"], "arguments":[{"category" : "Array", "type":"char", value:["h","e",{"category":"Object", "type":"Character", value:108}, "l", "o"]}]}
-> new String(new char[] {'H', 'e', 108, 'l', 'o'})
{"call":"new", "receiver":"java.lang.String", "signature":["char[]", "int", "int"], "arguments":[{"category" : "Array", "type":"char", value:["h","e",{"category":"Object", "type":"Character", value:108}, "l", "o"]}, 1, 2]}
-> new String(new char[] {'H', 'e', 108, 'l', 'o'}, 1, 2)
{"call":"new", "receiver":"java.lang.String", "signature":["java.lang.String"], "arguments":[{"call":"new", "receiver":"java.lang.String", "signature":["char[]"], "arguments":[{"category" : "Array", "type":"char", value:["h","e",{"category":"Object", "type":"Character", value:108}, "l", "o"]}]}]}
-> new String(new String(new char[] {'H', 'e', 108, 'l', 'o'}))

* Invoke a method

{"call":"invoke_method", "receiver":{"category":"Object", "type":"String", "value":"Meow"}, "name":"length"}
-> "Meow".length()
{"call":"invoke_method", "receiver":{"call":"new", "receiver":"com.miouyouyou.libraries.javon.ObjectLibrary"}, "name":"store", "signature":["java.lang.Object", "java.lang.Object"], "arguments":[{"category":"Object", "type":"String", "value":"A short cat"}, 10]}
-> new com.miouyouyou.libraries.javon.ObjectLibrary().store("A short cat", 10)


