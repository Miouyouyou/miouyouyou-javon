package tests.com.miouyouyou.libraries.javon;

import com.miouyouyou.libraries.javon.ResultDisplay;
import com.miouyouyou.libraries.javon.JSONParsers;
import com.miouyouyou.libraries.javon.ObjectLibrary;
import com.miouyouyou.libraries.helpers.QuickBoxedCaster;

import java.util.List;
import java.util.ArrayList;
import junit.framework.Assert;
import junit.framework.TestCase;

import org.json.JSONObject;
import org.json.JSONException;

public class ResultDisplayTest extends TestCase {

  public void testJsonNullValue() throws JSONException {
    String javon_null  = ResultDisplay.javon(null);
    String json_null_value = ResultDisplay.javon_null_value();
    Assert.assertEquals(javon_null, json_null_value);
    JSONObject jsoned_null_value = new JSONObject(javon_null);
    Assert.assertNull(JSONParsers.parse_argument(jsoned_null_value));
  }

  public void testJsonNumber() throws JSONException {
    String javon_with_number = ResultDisplay.javon(7);
    String javon_number = ResultDisplay.javon_number(7);
    Assert.assertEquals(javon_with_number, javon_number);

    Object[][] numeric_classes_and_numbers = new Object[][]
      {{Byte.class,  (byte) 10},
       {Short.class, (short) -741},
       {Character.class, (char) 7425},
       {Integer.class, -7894565},
       {Long.class, 3651669698712L},
       {Float.class, (float) 55123.661},
       {Double.class, 1132879.4687985D},
       {Float.class, Float.NaN},
       {Float.class, Float.POSITIVE_INFINITY},
       {Float.class, Float.NEGATIVE_INFINITY},
       {Double.class, Double.NaN},
       {Double.class, Double.POSITIVE_INFINITY},
       {Double.class, Double.NEGATIVE_INFINITY}};
    
    for (Object[] numeric_class_and_number : numeric_classes_and_numbers) {
      JSONObject jsoned_number =
        new JSONObject(ResultDisplay.javon(numeric_class_and_number[1]));
      try {
        Object parsed_number = JSONParsers.parse_argument(jsoned_number);
        Assert.assertEquals(numeric_class_and_number[1],
                            parsed_number);
        Assert.assertEquals(numeric_class_and_number[0],
                            parsed_number.getClass());
      } 
      catch (Exception e) { 
        throw new RuntimeException(jsoned_number.toString(), e); 
      }

    }

  }

  public void testEscapeString() {
    String before = "\"\"\"";
    String after  = "\\\"\\\"\\\"";
    Assert.assertEquals(after, ResultDisplay.escape_string(before));
  }

  public void testProperty() {
    /* Without quotes around JSON keys, some parsers fail to parse the provided
       JSON Object.
       For example, the default JSON parser in Ruby will not parse
       {nya: 1} but will parse {"nya": 1} */
    Assert.assertEquals("\"nya\": 1", 
                        ResultDisplay.property("nya", "1"));
    Assert.assertEquals("\"nya\": \"a\"",
                        ResultDisplay.property("nya", "\"a\""));
    Assert.assertEquals("\"nya\": \"\\\"a\\\"\"",
                        ResultDisplay.property("nya", "\"a\"", true));
  }

  public void testJsonString() throws JSONException {


    StringBuilder the_mega_string = new StringBuilder();
    for (char character = 0; character < 0xFFFF; character++) {
      the_mega_string.append(character);
    }

    /* The quotes in the second string are unbalanced on purpose */
    String[] original_strings = 
      new String[] {"a", "\"Miou\"\"", "", "true", "false", "null", 
                    the_mega_string.toString()};
    for (String original_string : original_strings) {
      Assert.assertEquals(ResultDisplay.javon(original_string),
                          ResultDisplay.javon_string_object(original_string));
      JSONObject jsoned_string = 
        new JSONObject(ResultDisplay.javon(original_string));
      Assert.assertEquals(original_string,
                          JSONParsers.parse_argument(jsoned_string));
    }
  }

  public void testJsonClass() throws JSONException {
    Class[] tested_classes =
      new Class[] {short.class, Void.class, Boolean.class, Void[].class,
                   java.util.Set[][][].class, Class.class, Class[][].class};
    for (Class tested_class : tested_classes) {
      Assert.assertEquals(ResultDisplay.javon(tested_class),
                          ResultDisplay.javon_class_object(tested_class));
      JSONObject jsoned_class =
        new JSONObject(ResultDisplay.javon(tested_class));
      Assert.assertEquals(tested_class,
                          JSONParsers.parse_argument(jsoned_class));
    }
  }

  public void testJsonCharacter() throws JSONException {
    Character[] characters =
      new Character[] {'€', '±', '¬', (char) 0, (char) 0x001f, 
                       (char) 0xd800, (char) 0xde85};
    for (Character character : characters) {
      Assert.assertEquals(ResultDisplay.javon(character),
                          ResultDisplay.javon_character_object(character));
      JSONObject jsoned_character =
        new JSONObject(ResultDisplay.javon(character));
      Assert.assertEquals(character,
                          JSONParsers.parse_argument(jsoned_character));
    }
  }

  public void testJsonBoolean() throws JSONException {
    Boolean[] flags =
      new Boolean[] {true, false};
    for (Boolean flag : flags) {
      Assert.assertEquals(ResultDisplay.javon(flag),
                          ResultDisplay.javon_boolean(flag));
      JSONObject jsoned_boolean =
        new JSONObject(ResultDisplay.javon(flag));
      Assert.assertEquals(flag,
                          JSONParsers.parse_argument(jsoned_boolean));
    }
  }

  public void testJsonArray() throws JSONException {
    Object[] objects = new Object[] 
      {null, 1598, Float.NaN, Double.POSITIVE_INFINITY, true, "true", "null",
       '€', (byte) 56, (char) 9863, (short) -5645, 5.894e10, "\"a\"", 
       Object[].class, Void[][].class, Double.NaN};
    Assert.assertEquals(ResultDisplay.javon(objects),
                        ResultDisplay.javon_array(objects));
    JSONObject jsoned_array = 
      new JSONObject(ResultDisplay.javon(objects));
    Object[] parsed_back_objects =
      (Object[]) JSONParsers.parse_argument(jsoned_array, new ObjectLibrary());
    Assert.assertEquals(jsoned_array.toString(), objects.length, 
                        parsed_back_objects.length);
    for (int index = 0; index < objects.length; index++) {
      Assert.assertEquals(objects[index],
                          parsed_back_objects[index]);
    }
  }

  public void testJsonReference() throws JSONException {
    ObjectLibrary reference_storage = new ObjectLibrary();
    java.util.List<String> stored_list = new java.util.ArrayList<String>();
    Object array_list_id = reference_storage.store(stored_list);
    JSONObject jsoned_reference = 
      new JSONObject(ResultDisplay.javon_reference_object(array_list_id, 
                                                          stored_list));

    Object parsed_reference = JSONParsers.parse_argument(jsoned_reference,
                                                         reference_storage);
    stored_list.add("Wan !");
    Assert.assertEquals("Wan !", ((List) parsed_reference).get(0));
  }

  public void testJavonException() throws JSONException {
    String simple_exception_message = "Something wrong happened !";
    Exception simple_exception = new java.lang.Exception(simple_exception_message);

    Assert.assertEquals(ResultDisplay.javon(simple_exception),
                        ResultDisplay.javon_exception(simple_exception));

    String jsoned_simple_exception_string = 
      ResultDisplay.javon_exception(simple_exception);
    JSONObject jsoned_simple_exception = 
      new JSONObject(jsoned_simple_exception_string);

    Assert.assertEquals("Exception",
                        jsoned_simple_exception.getString("category"));
    Assert.assertEquals(simple_exception_message,
                        jsoned_simple_exception.getString("message"));
    Assert.assertEquals("java.lang.Exception",
                        jsoned_simple_exception.getString("getClass"));
    Assert.assertEquals("thrown",
                        jsoned_simple_exception.getString("cause"));

    String complex_exception_message = "Oh no !";
    Exception complex_exception = 
      new RuntimeException(complex_exception_message, simple_exception);
    JSONObject jsoned_complex_exception =
      new JSONObject(ResultDisplay.javon_exception(complex_exception));

    Assert.assertEquals("Exception",
                        jsoned_complex_exception.getString("category"));
    Assert.assertEquals(complex_exception_message,
                        jsoned_complex_exception.getString("message"));
    Assert.assertEquals("java.lang.RuntimeException",
                        jsoned_complex_exception.getString("getClass"));
    Assert.assertEquals("java.lang.Exception",
                        jsoned_complex_exception.getString("cause"));

    Exception super_exception =
      new RuntimeException("wat", complex_exception);
    JSONObject jsoned_super_exception =
      new JSONObject(ResultDisplay.javon_exception(super_exception));
    
    Assert.assertEquals("java.lang.Exception",
                        jsoned_complex_exception.getString("cause"));
  }

  public void testHasJavonRepresentation() {
    Object[] representable_objects =
      {true, false, (byte) 12, (short) 781, (char) '±', -7856,
       1234684697L, (float) 123.12, (double) 789.27e12, 
       Float.NaN, Double.POSITIVE_INFINITY, Float.NEGATIVE_INFINITY,
       "hello", Number.class, new int[] {1}, new Double[1], 
       new Class[] {Long.class, CharSequence.class}, new boolean[2],
       new Boolean[3], new char[] {'a', 'b', 'c'}, new Character[4],
       new String[][] {{"hi", "there"},{"How", "are", "you"}} };
    for (Object representable_object : representable_objects) {
      Assert.assertTrue(representable_object.toString(),
                        ResultDisplay.has_javon_representation(representable_object));
    }
    
  }
}
