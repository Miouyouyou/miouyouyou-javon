package tests.com.miouyouyou.libraries.javon.extractors;

import com.miouyouyou.libraries.javon.extractors.JavonArgumentsExtractor;
import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;
import com.miouyouyou.libraries.javon.ObjectLibrary;
import static com.miouyouyou.libraries.json.JSONNestedArraysToArray.to_array;

import java.lang.reflect.Array;

import org.json.JSONArray;

import junit.framework.Assert;
import junit.framework.TestCase;

public class JavonArgumentsExtractorTest extends TestCase {

  ObjectStorage object_storage = new ObjectLibrary();
  JSONArray string_reference_array = null;
  String[] expected_string_array = null;
  JSONArray nested_string_reference_array = null;
  String[][] expected_nested_string_array = null;
  JSONArray long_reference_array = null;
  long[] expected_long_array = null;
  Long[] expected_Long_array = null;
  JSONArray objectlib_reference_array = null;
  ObjectStorage[] expected_objectlib_array = null;
  JSONArray nested_objectlib_reference_array = null;
  ObjectLibrary[][] expected_nested_objectlib_array = null;
  JSONArray objectlib_w_null_and_invalid_reference_array = null;
  ObjectLibrary[] expected_objectlib_with_null_array = null;
  

  public String ref(Object id) {
    return String.format("{\"category\" : \"Object\", \"type\" : "+
                         "\"Reference\", \"value\" : %s}", id.toString());
  }

  public void setUp() throws Exception {
    super.setUp();
    Object first_string  = this.object_storage.store("ABC");
    Object second_string = this.object_storage.store("DEF", 1);
    Object first_long   = this.object_storage.store(-147L, 2);
    Object second_long = this.object_storage.store(751L, 3);
    Object first_objectlib = 
      this.object_storage.store(new ObjectLibrary(), 4);
    Object second_objectlib = 
      this.object_storage.store(new ObjectLibrary(), 5);
    Object null_value =
      this.object_storage.store(null, 6);

    this.expected_string_array =
      new String[] {"ABC", "DEF"};
    this.string_reference_array = 
      new JSONArray(String.format("[%s, %s]",
                                  ref(first_string),
                                  ref(second_string)));

    this.expected_nested_string_array =
      new String[][] {{"ABC", "DEF"},
                      {"ABC", "ABC", "ABC"},
                      {"DEF"}};
    String nested_reference_array_string = 
      String.format("[[%s, %s], [%s, %s, %s], [%s]]",
                    ref(first_string),
                    ref(second_string),
                    ref(first_string),
                    ref(first_string),
                    ref(first_string),
                    ref(second_string));

    this.nested_string_reference_array =
      new JSONArray(nested_reference_array_string);

    this.expected_long_array =
      new long[] {-147L, 751L};
    this.expected_Long_array =
      new Long[] {-147L, 751L};
    this.long_reference_array =
      new JSONArray(String.format("[%s, %s]",
                                  ref(first_long),
                                  ref(second_long)));

    this.expected_objectlib_array =
      new ObjectStorage[] 
      {(ObjectStorage) object_storage.retrieve(first_objectlib),
       (ObjectStorage) object_storage.retrieve(second_objectlib)};
    this.objectlib_reference_array =
      new JSONArray(String.format("[%s, %s]",
                                  ref(first_objectlib),
                                  ref(second_objectlib)));

    this.expected_nested_objectlib_array =
      new ObjectLibrary[][]
      {{(ObjectLibrary) object_storage.retrieve(first_objectlib),
        (ObjectLibrary) object_storage.retrieve(second_objectlib),
        (ObjectLibrary) object_storage.retrieve(second_objectlib),
        (ObjectLibrary) object_storage.retrieve(first_objectlib)},
       {(ObjectLibrary) object_storage.retrieve(second_objectlib),
        (ObjectLibrary) object_storage.retrieve(first_objectlib)}
      };

    String nested_objectlib_reference_array_string =
      String.format("[[%s, %s, %s, %s], [%s, %s]]",
                    ref(first_objectlib),
                    ref(second_objectlib),
                    ref(second_objectlib),
                    ref(first_objectlib),
                    ref(second_objectlib),
                    ref(first_objectlib));
    this.nested_objectlib_reference_array =
      new JSONArray(nested_objectlib_reference_array_string);

    Object invalid_reference = 700;
    this.expected_objectlib_with_null_array =
      new ObjectLibrary[]
      {(ObjectLibrary) object_storage.retrieve(first_objectlib),
       (ObjectLibrary) object_storage.retrieve(second_objectlib),
       (ObjectLibrary) object_storage.retrieve(null_value)};
    this.objectlib_w_null_and_invalid_reference_array =
      new JSONArray(String.format("[%s, %s, %s, %s]",
                                  ref(first_objectlib),
                                  ref(invalid_reference),
                                  ref(second_objectlib),
                                  ref(null_value)));

  }

  public void testExtract() throws Exception {
    JavonArgumentsExtractor string_extractor = 
      new JavonArgumentsExtractor(String.class, this.object_storage);
    String[] string_array =
      (String[]) string_extractor.extract_values(this.string_reference_array);
    String[][] nested_string_array =
      (String[][]) to_array(this.nested_string_reference_array, 
                            string_extractor);

   
    Assert.assertEquals(expected_string_array.length,
                        string_array.length);
                        
    checkArrays(this.expected_string_array,
                string_array);
    checkArrays(this.expected_nested_string_array,
                nested_string_array);
    

    /* While long can be automatically boxed to Long and vice-versa,
       long[] and Long[] are not compatible types.
       This means that a method awaiting a 'long' can receive a 'Long' 
       but a method awaiting a 'long[]' cannot receive a 'Long[]'. */
    JavonArgumentsExtractor long_extractor =
      new JavonArgumentsExtractor(long.class, this.object_storage);
    long[] long_array =
      (long[]) long_extractor.extract_values(this.long_reference_array);

    JavonArgumentsExtractor Long_extractor =
      new JavonArgumentsExtractor(Long.class, this.object_storage);
    Long[] Long_array =
      (Long[]) Long_extractor.extract_values(this.long_reference_array);
    checkArrays(this.expected_long_array, long_array);
    checkArrays(this.expected_Long_array, Long_array);

    JavonArgumentsExtractor object_storage_extractor =
      new JavonArgumentsExtractor(ObjectStorage.class, this.object_storage);
    ObjectStorage[] object_storage_array =
      (ObjectStorage[]) object_storage_extractor.
      extract_values(this.objectlib_reference_array);

    JavonArgumentsExtractor object_library_extractor =
      new JavonArgumentsExtractor(ObjectLibrary.class, this.object_storage);
    ObjectLibrary[][] nested_object_library_array =
      (ObjectLibrary[][]) to_array(this.nested_objectlib_reference_array,
                                   object_library_extractor);
    ObjectLibrary[] object_library_with_null_array =
      (ObjectLibrary[]) to_array(this.objectlib_w_null_and_invalid_reference_array,
                                 object_library_extractor);

    checkArrays(this.expected_objectlib_array, 
                object_storage_array);
    checkArrays(this.expected_nested_objectlib_array, 
                nested_object_library_array);
    checkArrays(this.expected_objectlib_with_null_array,
                object_library_with_null_array);

    

  }

  /* Overloaded to avoid long values being converted to Long automagically
     (given the implementation of checkArrays(Object, Object)) */
  public void checkArrays(long[] first_long_array, 
                          long[] second_long_array) {
    Assert.assertEquals(first_long_array.length,
                        second_long_array.length);
    for( int index = 0; index < first_long_array.length; index++ ) {
      Assert.assertEquals(first_long_array[index],
                          second_long_array[index]);
    }
  }

  public void checkArrays(Object first_array,
                          Object second_array) {
    Object object_from_first  = null;
    Object object_from_second = null;
    int first_length  = Array.getLength(first_array);
    int second_length = Array.getLength(second_array);
    Assert.assertEquals(first_length, second_length);

    for (int index = 0; index < first_length; index++) {
      object_from_first  = Array.get(first_array, index);
      object_from_second = Array.get(second_array, index);
      if (object_from_first != null &&
          object_from_first.getClass().isArray()) {
        checkArrays(object_from_first,
                    object_from_second); }
      else { Assert.assertEquals(object_from_first,
                                 object_from_second); }
    }
  }


    
}
