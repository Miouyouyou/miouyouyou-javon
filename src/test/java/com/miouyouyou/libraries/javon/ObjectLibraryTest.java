package tests.com.miouyouyou.libraries.javon;

import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;
import com.miouyouyou.libraries.javon.ObjectLibrary;

// Test object
import java.text.RuleBasedCollator;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ObjectLibraryTest extends TestCase {
  public void testStoreAndRetrieve() throws Exception {
    ObjectStorage storage = new ObjectLibrary();
    Object integer_object = (Integer) 1;
    Object integer_object_id = storage.store(integer_object);
    Object retrieved_object = storage.retrieve(integer_object_id);
    Assert.assertEquals(integer_object,
                        retrieved_object);

    // Overwriting the last object with a new object
    Object string_array_object = new String[] {"a", "b"};
    storage.store(string_array_object, integer_object_id);
    retrieved_object = storage.retrieve(integer_object_id);
    Assert.assertEquals(string_array_object,
                        retrieved_object);

    // Storing a new object. The last object should still be there.
    Object test_object  = new RuleBasedCollator("< a< b< c< d");
    Object test_object_id = storage.store(test_object);
    Assert.assertFalse(integer_object_id  == test_object_id);
    retrieved_object = storage.retrieve(test_object_id);
    Assert.assertEquals(test_object,
                        retrieved_object);
  }
}
