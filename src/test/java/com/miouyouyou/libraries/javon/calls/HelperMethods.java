package tests.com.miouyouyou.libraries.javon.calls;

import static com.miouyouyou.libraries.helpers.JavaStringHelpers.list;

public class HelperMethods {
  public static String generate_call(String... properties) {
    return String.format("{ %s }", list(",", properties));
  }

  public static String receiver(String literal_class_name) {
    return String.format("\"receiver\" : %s", literal_class_name);
  }

  public static String signature(String... parameters) {
    return String.format("\"signature\" : [ %s ]", 
                         list(",", parameters));
  }

  public static String arguments(String... argumentlist) {
    return String.format("\"arguments\" : [ %s ]",
                         list(",", argumentlist));
  }

  public static String name(String name) {
    return String.format("\"name\" : %s", name);
  }
}
