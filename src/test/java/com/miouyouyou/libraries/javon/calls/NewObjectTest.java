package tests.com.miouyouyou.libraries.javon.calls;

import tests.com.miouyouyou.libraries.javon.calls.TestClass;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.generate_call;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.receiver;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.signature;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.arguments;


import com.miouyouyou.libraries.javon.calls.NewObject;
import com.miouyouyou.libraries.javon.ObjectLibrary;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

public class NewObjectTest extends TestCase {
  public void testExecute() throws Exception {
    ObjectLibrary reference_storage = new ObjectLibrary();
    String testclass_literalname =
      "\"tests.com.miouyouyou.libraries.javon.calls.TestClass\"";
    String simple_json_call = 
      generate_call(receiver(testclass_literalname));
      // "{ \"receiver\" : \"tests.com.miouyouyou.libraries.javon.calls.TestClass\"}";
    String call_with_string_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.String\""),
                    arguments("\"success\""));
    String call_with_strings_arguments =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.String[]\""),
                    arguments("{ category : \"Array\", "+
                              "  type  : \"java.lang.String\", "+
                              "  value : [\"suc\", \"cess\"] }"));
    String call_with_byte_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"byte\""),
                    arguments("{ category : \"Number\","+
                              "  type  : \"byte\","+
                              "  value : 100 }"));
    String call_with_Byte_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.Byte\""),
                    arguments("{ category : \"Number\","+
                              "  type  : \"java.lang.Byte\","+
                              "  value : 100 }"));
    String call_with_byte_array =
      generate_call(receiver(testclass_literalname),
                    signature("\"byte[]\""),
                    arguments("{ category : \"Array\","+
                              "  type  : \"byte\","+
                              "  value : [70, { category : \"Number\","+
                              "                 type     : \"long\","+
                              "                 value    : 35}]}"));
    String call_with_Byte_array =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.Byte[]\""),
                    arguments("{ category : \"Array\","+
                              "  type  : \"java.lang.Byte\","+
                              "  value : [40, { category : \"Number\","+
                              "                 type     : \"java.lang.Short\","+
                              "                 value    : 70}]}"));

    String call_with_char_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"char\""),
                    arguments("{ category : \"Number\","+
                              "  type     : \"char\","+
                              "  value    : \"A\"}"));
    String call_with_Character_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.Character\""),
                    arguments("{ category : \"Object\","+
                              "  type     : \"Character\","+
                              "  value    : 13203}"));

    String call_with_char_array =
      generate_call(receiver(testclass_literalname),
                    signature("\"char[]\""),
                    arguments("{ category : \"Array\","+
                              "  type : \"char\","+
                              "  value : [51, \".\", { category : \"Object\","+
                              "                        type     : \"Character\","+
                              "                        value    : \"2\"},"+
                              "                      { category : \"Number\","+
                              "                        type     : \"java.lang.Character\","+
                              "                        value    : 13203}]}"));
    String call_with_Character_array =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.Character[]\""),
                    arguments("{ category : \"Array\","+
                              "  type : \"java.lang.Character\","+
                              "  value : [51.1, null, \"abc\", \".\", "+
                              "           true, false, \"null\","+
                              "          { category : \"Object\","+
                              "            type     : \"Character\","+
                              "            value    : \"2\"},"+
                              "          { category : \"Number\","+
                              "            type     : \"java.lang.Double\","+
                              "            value    : 13203}]}"));

    String call_with_boolean_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"boolean\""),
                    arguments("true"));

    String call_with_Boolean_argument =
      generate_call(receiver(testclass_literalname),
                    signature("\"java.lang.Boolean\""),
                    arguments("true"));

    Object array_list = new NewObject("{ receiver : \"java.util.ArrayList\"}",
                                      reference_storage).execute();
    reference_storage.store(array_list, 0);

    ObjectLibrary special_storage = new ObjectLibrary();
    reference_storage.store(special_storage, 1);
    String call_with_multiple_arguments =
      generate_call(receiver(testclass_literalname),
                    signature("\"byte\", \"java.lang.Short[]\", \"java.lang.Character[][]\","+
                              "\"java.lang.String\", \"java.util.List\","+
                              "\"com.miouyouyou.libraries.javon.interfaces.ObjectStorage\""),
                    arguments("{ category : \"Number\", type : \"byte\", value : 15 },"+
                              "{ category : \"Array\", type : \"java.lang.Short\", value : [10, 5]},"+
                              "{ category : \"Array\", type : \"java.lang.Character\","+
                              "  value : [[\"s\", \"u\", \"c\", \"c\"], [\"e\", \"s\", \"s\"]]},"+
                              "\"Success !\",",
                              "{ category : \"Object\", type : \"Reference\", value : 0},",
                              "{ category : \"Object\", type : \"Reference\", value : 1}"));
                              

    assertEquals("success", 
                 ((TestClass) new NewObject(simple_json_call, reference_storage).execute()).test());
    assertEquals("success",
                 ((TestClass) new NewObject(call_with_string_argument, reference_storage).execute()).return_string());
    assertEquals("success",
                 ((TestClass) new NewObject(call_with_strings_arguments, reference_storage).execute()).return_strings());
    assertEquals((byte) 100,
                 ((TestClass) new NewObject(call_with_byte_argument, reference_storage).execute()).return_byte());
    assertEquals((Byte) ((byte) 100),
                 ((TestClass) new NewObject(call_with_Byte_argument, reference_storage).execute()).return_Byte());
    assertEquals((byte) 105,
                 ((TestClass) new NewObject(call_with_byte_array, reference_storage).execute()).return_bytes());
    assertEquals((Byte) ((byte) 110),
                 ((TestClass) new NewObject(call_with_Byte_array, reference_storage).execute()).return_Bytes());
    assertEquals("3.2㎓", ((TestClass) new NewObject(call_with_char_array, reference_storage).execute()).return_chars());
    assertEquals("3.2㎓", ((TestClass) new NewObject(call_with_Character_array, reference_storage).execute()).return_Characters());
    assertEquals('A', ((TestClass) new NewObject(call_with_char_argument, reference_storage).execute()).return_char());
    assertEquals((Character) '㎓', ((TestClass) new NewObject(call_with_Character_argument, reference_storage).execute()).return_Character());
    assertEquals(true, ((TestClass) new NewObject(call_with_boolean_argument, reference_storage).execute()).return_boolean());
    assertEquals((Boolean) true, ((TestClass) new NewObject(call_with_Boolean_argument, reference_storage).execute()).return_Boolean());

    /* arraylist and special_storage are passed to the TestClass constructor
       AND then set to a specific value outside
       AND THEN, these specific values are tested during the call to 
       "return_multiple_args_success()" executed on the instantiated TestClass
       object.
       That ensures that "Reference" "Object"s are really about references and
       not pseudo copies ! */
    TestClass testclass_object = 
      (TestClass) new NewObject(call_with_multiple_arguments,
                                reference_storage).execute();
    ((ArrayList) array_list).add("I like");
    special_storage.store("Muffins", 7);
    assertEquals("Success !!", testclass_object.return_multiple_args_success());
    
  }


}
