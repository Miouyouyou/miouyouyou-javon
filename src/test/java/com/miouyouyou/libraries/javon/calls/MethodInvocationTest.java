package tests.com.miouyouyou.libraries.javon.calls;

import com.miouyouyou.libraries.javon.calls.MethodInvocation;
import com.miouyouyou.libraries.javon.calls.NewObject;
import com.miouyouyou.libraries.javon.ObjectLibrary;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.generate_call;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.receiver;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.signature;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.arguments;
import static tests.com.miouyouyou.libraries.javon.calls.HelperMethods.name;

import java.util.ArrayList;
import java.util.HashMap;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.json.JSONObject;

public class MethodInvocationTest extends TestCase {

  public ObjectLibrary reference_storage;

  public void setUp() throws Exception {
    super.setUp();
    this.reference_storage = new ObjectLibrary();
  }
  public void testStaticMethodInvocation() {

    String testclass_identifier = 
      "{ category : \"Object\", type : \"Class\", "+
      "  value : \"tests.com.miouyouyou.libraries.javon.calls.TestClass\"}";

    String static_no_args_method =
      generate_call(receiver(testclass_identifier),
                    name("no_args_method"));
    String string_method =
      generate_call(receiver(testclass_identifier),
                    name("return_provided_value"),
                    signature("java.lang.String"),
                    arguments("\"Success !\""));

    ArrayList<String> a = new ArrayList<String>();
    reference_storage.store(a, 0);
    String overloaded_list_method =
      generate_call(receiver(testclass_identifier),
                    name("return_provided_value"),
                    signature("java.util.List"),
                    arguments("{ category : \"Object\","+
                              "  type : \"Reference\","+
                              "  value : 0}"));
    
    String strings_method =
      generate_call(receiver(testclass_identifier),
                    name("return_concatenated_strings"),
                    signature("java.lang.String, java.lang.String"),
                    arguments("\"Suc\", \"cess !!\""));

    String string_array_method =
      generate_call(receiver(testclass_identifier),
                    name("return_concatenated_strings"),
                    signature("\"java.lang.String[]\""),
                    arguments("{ category : \"Array\","+
                              "  type : \"java.lang.String\","+
                              "  value : [\"Succ\", \"ess !?\"]}"));

    String expect_null_method =
      generate_call(receiver(testclass_identifier),
                    name("return_true_if_arg_is_null"),
                    signature("java.util.Map"),
                    arguments("{ category : \"Object\", type : \"null\"}"));

    String boolean_varargs_method =
      generate_call(receiver(testclass_identifier),
                    name("return_true_if_all_true"),
                    signature("\"boolean[]\""),
                    arguments("{ category : \"Array\", type : \"boolean\","+
                              "  value : [true, true, true]}"));


    Assert.assertEquals("Success",
                        new MethodInvocation(static_no_args_method,
                                             this.reference_storage).
                        execute());
    Assert.assertEquals("Success !",
                        new MethodInvocation(string_method,
                                             this.reference_storage).
                        execute());
    a.add("Success.");
    Assert.assertEquals("Success.",
                        new MethodInvocation(overloaded_list_method,
                                             this.reference_storage).
                        execute());
    Assert.assertEquals("Success !!",
                        new MethodInvocation(strings_method,
                                             this.reference_storage).
                        execute());
    Assert.assertEquals("Success !?",
                        new MethodInvocation(string_array_method,
                                             this.reference_storage).
                        execute());
    Assert.assertTrue((Boolean) new MethodInvocation(expect_null_method,
                                                     this.reference_storage).
                      execute());
    Assert.assertTrue((Boolean) new MethodInvocation(boolean_varargs_method,
                                                     this.reference_storage).
                      execute());
    
  }

  public void testInstanceMethodInvocation() {
    TestClass testclass_object = 
      (TestClass) 
      new NewObject("{ \"receiver\" : \"tests.com.miouyouyou.libraries.javon."+
                    "calls.TestClass\"}", reference_storage).execute();
    reference_storage.store(testclass_object, 1);
    String testclass_reference = 
      "{ category : \"Object\", type : \"Reference\", value : 1}";


    String no_args_method =
      generate_call(receiver(testclass_reference),
                    name("dynamic_no_args_method"));
    String overloaded_Character_method =
      generate_call(receiver(testclass_reference),
                    name("dynamic_return_provided_value"),
                    signature("java.lang.Character"),
                    arguments("{category : \"Object\", type : \"Character\","+
                              " value : \"€\"}"));

    HashMap<String,String> cookies_map = new HashMap();
    reference_storage.store(cookies_map, 2);
    String overloaded_map_method =
      generate_call(receiver(testclass_reference),
                    name("dynamic_return_provided_value"),
                    signature("java.util.Map"),
                    arguments("{ category : \"Object\", type : \"Reference\","+
                              " value : 2}"));

    String Character_varargs_method = 
      generate_call(receiver(testclass_reference),
                    name("dynamic_return_concatenated_characters"),
                    signature("\"java.lang.Character[]\""),
                    arguments("{ category : \"Array\", "+
                              "  type : \"java.lang.Character\","+
                              "  value : [\"\\u231b\", \"5\","+
                              "           { category : \"Number\","+
                              "             type     : \"char\","+
                              "             value    : 8242},"+
                              "           { category : \"Object\","+
                              "             type     : \"Character\","+
                              "             value    : 9000}]}"));
                              
    String charsequence_array_method =
      generate_call(receiver(testclass_reference),
                    name("dynamic_return_concatenated_characters"),
                    signature("\"java.lang.CharSequence[]\""),
                    arguments("{ category : \"Array\", "+
                              "  type : \"java.lang.CharSequence\","+
                              "  value : [\"\\u231b\", \" Please wait 5′\","+
                              "           \". Don't touch the \\u2328\"]}"));

    String empty_map_method =
      generate_call(receiver(testclass_reference),
                    name("dynamic_return_false_if_arg_is_empty"),
                    signature("java.util.Map"),
                    arguments("{ category : \"Object\", "+
                              "  type : \"Reference\", "+
                              "  value : 2}"));

    String null_boolean_array_method =
      generate_call(receiver(testclass_reference),
                    name("dynamic_true_if_all_null_and_size_two"),
                    signature("\"java.lang.Boolean[]\""),
                    arguments("{ category : \"Array\", "+
                              "  type : \"java.lang.Boolean\", "+
                              "  value : [{ category : \"Object\", "+
                              "             type : \"null\"}, "+
                              "           { category : \"Object\", "+
                              "             type : \"null\"}]}"));

    Assert.assertEquals((Short) ((short) 9874),
                        new MethodInvocation(no_args_method,
                                             this.reference_storage).
                        execute());
    Assert.assertEquals((Character) '€',
                        new MethodInvocation(overloaded_Character_method,
                                             this.reference_storage).
                        execute());

    cookies_map.put("I like :", "Cookies");
    Assert.assertEquals("Cookies",
                        new MethodInvocation(overloaded_map_method,
                                             this.reference_storage).
                        execute());
    Assert.assertEquals("⌛5′⌨",
                        new MethodInvocation(Character_varargs_method,
                                             this.reference_storage).
                        execute());

    Assert.assertEquals("⌛ Please wait 5′. Don't touch the ⌨",
                        new MethodInvocation(charsequence_array_method,
                                             this.reference_storage).
                        execute());

    cookies_map.clear();
    
    Assert.assertFalse((Boolean) new MethodInvocation(empty_map_method,
                                                      this.reference_storage).
                       execute());
    Assert.assertTrue((Boolean) new MethodInvocation(null_boolean_array_method,
                                                     this.reference_storage).
                      execute());
    
  }

  public void testMixedInvocation() {

    ObjectLibrary reference_storage = new ObjectLibrary();

    String testclass_new_object_call =
      generate_call("call : \"new\"",
                    receiver("\"tests.com.miouyouyou.libraries.javon.calls.TestClass\""),
                    signature("\"java.lang.String\""),
                    arguments("\"success\""));

    String return_string_on_jsoned_testclass =
      generate_call(receiver(testclass_new_object_call),
                    name("return_string"));
      
    String Character_varargs_on_jsoned_testclass =
      generate_call(receiver(testclass_new_object_call),
                    name("dynamic_return_concatenated_characters"),
                    signature("\"java.lang.Character[]\""),
                    arguments("{ category : \"Array\", "+
                              "  type : \"java.lang.Character\","+
                              "  value : [\"\\u231b\", \"5\","+
                              "           { category : \"Number\","+
                              "             type     : \"char\","+
                              "             value    : 8242},"+
                              "           { category : \"Object\","+
                              "             type     : \"Character\","+
                              "             value    : 9000}]}"));

    Assert.assertEquals("success",
                        new MethodInvocation(return_string_on_jsoned_testclass,
                                             reference_storage).execute());

    Assert.assertEquals("⌛5′⌨",
                        new MethodInvocation(Character_varargs_on_jsoned_testclass,
                                             reference_storage).execute());
                              
  }
}
