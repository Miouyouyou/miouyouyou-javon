package tests.com.miouyouyou.libraries.javon.calls;

import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

public class TestClass {
  private String      constructor_string = null;
  private String[]    constructor_strings = null;
  private byte        constructor_byte_value = 0;
  private Byte        constructor_Byte_value = null;
  private byte[]      constructor_byte_values = null;
  private Byte[]      constructor_Byte_values = null;
  private char        constructor_char = 0;
  private Character   constructor_Character = null;
  private char[]      constructor_chars = null;
  private Character[] constructor_Characters = null;
  private boolean     constructor_boolean = false;
  private Boolean     constructor_Boolean = null;

  private byte          constructor_multiple_args_number = 0;
  private Short[]       constructor_multiple_args_short_array = null;
  private Character[][] constructor_multiple_args_characters_arrays = null;
  private String        constructor_multiple_args_string = null;
  private List<String>  constructor_multiple_args_list = null;
  private ObjectStorage constructor_multiple_args_object_storage = null;
  /*
byte number, Short[] short_array, 
                   Character[][] characters_arrays,
                   String string,
                   ArrayList<String> string,
                   ObjectStorage object_storage
   */

  public TestClass() { }
  public TestClass(String string) { this.constructor_string = string; }
  public TestClass(String... strings) { this.constructor_strings = strings; }
  public TestClass(byte number) { this.constructor_byte_value = number; }
  public TestClass(Byte number) { this.constructor_Byte_value = number; }
  public TestClass(byte[] numbers) { this.constructor_byte_values = numbers; }
  public TestClass(Byte[] numbers) { this.constructor_Byte_values = numbers; }
  public TestClass(char[] characters) { this.constructor_chars = characters; }
  public TestClass(Character[] characters) { 
    this.constructor_Characters = characters;
  }
  public TestClass(char character) { this.constructor_char = character; }
  public TestClass(Character character) { 
    this.constructor_Character = character;
  }
  public TestClass(boolean flag) { this.constructor_boolean = flag; }
  public TestClass(Boolean flag) { this.constructor_Boolean = flag; }

  /* ObjectStorage is there because I need to test instantiation with an 
     Object outside the "java.*" namespace. */
  public TestClass(byte number, Short[] short_array, 
                   Character[][] characters_arrays,
                   String string,
                   List<String> list,
                   ObjectStorage object_storage) {
    this.constructor_multiple_args_number = number;
    this.constructor_multiple_args_short_array = short_array;
    this.constructor_multiple_args_characters_arrays = characters_arrays;
    this.constructor_multiple_args_string = string;
    this.constructor_multiple_args_list = list;
    this.constructor_multiple_args_object_storage = object_storage;
  }

  public String test() { return "success"; }
  public String return_string() { 
    return this.constructor_string;
  }
  public String return_strings() {
    StringBuilder string_builder = new StringBuilder();
    for (String string : this.constructor_strings) {
      string_builder.append(string);
    }
    return string_builder.toString().intern();
  }
  public byte return_byte() {
    return this.constructor_byte_value;
  }
  public Byte return_Byte() {
    return this.constructor_Byte_value;
  }
  public byte return_bytes() {
    byte result = 0;
    for (byte number : this.constructor_byte_values) {
      result += number;
    }
    return result;
  }
  public Byte return_Bytes() {
    /* The little cast in the loop code needs a rant because... fuck you JAVA !
       The following code doesn't compile :
       Byte a = 1;
       Byte b = 2;
       Byte c = a + b;
       Compiler error :
       -----
       incompatible types
       found   : int
       required: java.lang.Byte
       Byte c = a + b;
       -----
       SERIOUSLY !? ... REALLY !? WHERE DO YOU SEE AN INT !? I didn't put any
       fucking int in this code !
       Trying to correct this with Byte c = (Byte) (a + b); result in the
       same error...
       You have to do : Byte c = (byte) (a + b);
       Boxed primitives below Integer are just broken... If you cannot make a
       fucking addition without casting back the result into the primitive type
       represented by the boxed primitive class, it's broken... */

    Byte result = 0;
    for (Byte number : this.constructor_Byte_values) {
      result = (byte) (result + number);
    }
    return result;
  }

  public char return_char() {
    return this.constructor_char;
  }

  public Character return_Character() {
    return this.constructor_Character;
  }

  public String return_chars() {
    return new String(this.constructor_chars).intern();
  }

  public String return_Characters() {
    char[] characters = new char[this.constructor_Characters.length];
    for (int index = 0; index < constructor_Characters.length; index++) {
      characters[index] = this.constructor_Characters[index];
    }
    return new String(characters).intern();
  }
  public boolean return_boolean() {
    return this.constructor_boolean;
  }
  public Boolean return_Boolean() {
    return this.constructor_Boolean;
  }
  public String return_multiple_args_success() {

    Short short_array_total = 0;
    for (Short value : this.constructor_multiple_args_short_array) {
      short_array_total = (short) (short_array_total + value);
    }

    char[] characters_from_nested_array = 
      new char[this.constructor_multiple_args_characters_arrays[0].length +
               this.constructor_multiple_args_characters_arrays[1].length];
    int index = 0;
    for (Character[] characters : this.constructor_multiple_args_characters_arrays) {
      for (Character character : characters) {
        characters_from_nested_array[index++] = character;
      }
    }
    String string_from_nested_array = new String(characters_from_nested_array);
    
    Assert.assertEquals((byte) 15, this.constructor_multiple_args_number);
    Assert.assertEquals((Short) ((short) 15), short_array_total);
    Assert.assertEquals("success", string_from_nested_array);
    Assert.assertEquals("Success !", this.constructor_multiple_args_string);
    Assert.assertEquals("I like", this.constructor_multiple_args_list.get(0));
    Assert.assertEquals("Muffins", this.constructor_multiple_args_object_storage.retrieve(7));

    return("Success !!");
    
    
  }

  public static String no_args_method() {
    return "Success";
  }

  public static String return_provided_value(String string) {
    return string;
  }

  public static String return_provided_value(List<String> string_list) {
    return string_list.get(0);
  }

  public static String return_concatenated_strings(String first_string,
                                                   String second_string) {
    return String.format("%s%s", first_string, second_string);
  }

  public static String return_concatenated_strings(String[] strings) {
    StringBuilder string_builder = new StringBuilder();
    for (String string : strings) { string_builder.append(string); }
    return string_builder.toString();
  }

  public static boolean return_true_if_arg_is_null(Map<Class,Double> null_map) {
    return (null_map == null);
  }

  public static boolean return_true_if_all_true(boolean... flags) {
    for (boolean flag : flags) {
      if (flag == false) { return false; }
    }
    return true;
  }
  
  public Short dynamic_no_args_method() {
    return (short) 9874;
  }

  public Character dynamic_return_provided_value(Character character) {
    return character;
  }

  public String dynamic_return_provided_value(Map<String,String> map) {
    return map.get("I like :");
  }

  public String dynamic_return_concatenated_characters(Character... characters) {
    StringBuilder builder = new StringBuilder();
    for (Character character : characters) {
      builder.append((char) character);
    }
    return builder.toString();
  }

  public String dynamic_return_concatenated_characters(CharSequence[] sequences) {
    StringBuilder string_builder = new StringBuilder();
    for (CharSequence sequence : sequences) { string_builder.append(sequence); }
    return string_builder.toString();
  }

  public boolean dynamic_return_false_if_arg_is_empty(Map<Class,Double> empty_map) {
    return !(empty_map.isEmpty());
  }

  public boolean dynamic_true_if_all_null_and_size_two(Boolean... flags) {
    if (flags.length != 2) { return false; }
    for (Boolean flag : flags) {
      if (flag != null) { return false; }
    }
    return true;
  }
}
