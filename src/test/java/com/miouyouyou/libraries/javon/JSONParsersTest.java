package tests.com.miouyouyou.libraries.javon;

import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;
import com.miouyouyou.libraries.javon.ObjectLibrary;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_signature;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_numeric_primitive;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_class;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_string;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_argument;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_reference;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_array;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_external_argument;

import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;

import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

import org.json.JSONObject;

public class JSONParsersTest extends TestCase {
  public void testParseSignature() throws Exception {
    String json_signature = 
      "{signature : [\"int[]\", \"java.lang.String\", \"java.lang.Void\", "+
      "\"java.lang.String[][]\"]}";
    JSONObject signature = 
      new JSONObject(json_signature);
    Class[] parsed_signature = parse_signature(signature);
    assertEquals(int[].class, parsed_signature[0]);
    assertEquals(String.class, parsed_signature[1]);
    assertEquals(Void.class, parsed_signature[2]);
    assertEquals(String[][].class, parsed_signature[3]);
  }

  public void testParsePrimitives() throws Exception {
    String value_string = "100";
    byte value = Byte.parseByte(value_string);
    Map<Class,Object[]> primitive_types_and_values =
      new HashMap<Class,Object[]>();
    primitive_types_and_values
      .put(Byte.class, new Object[] {"byte", "java.lang.Byte",
                                     (byte) value,
                                     (byte) Double.NaN,
                                     (byte) Double.POSITIVE_INFINITY,
                                     (byte) Double.NEGATIVE_INFINITY});
    primitive_types_and_values
      .put(Short.class, new Object[] {"short", "java.lang.Short",
                                      (short) value,
                                      (short) Double.NaN,
                                      (short) Double.POSITIVE_INFINITY,
                                      (short) Double.NEGATIVE_INFINITY});
    primitive_types_and_values
      .put(Character.class, new Object[] {"char", "java.lang.Character",
                                          (char) value,
                                          (char) Double.NaN,
                                          (char) Double.POSITIVE_INFINITY,
                                          (char) Double.NEGATIVE_INFINITY});
    primitive_types_and_values
      .put(Integer.class, new Object[] {"int", "java.lang.Integer",
                                        (int) value,
                                        (int) Double.NaN,
                                        (int) Double.POSITIVE_INFINITY,
                                        (int) Double.NEGATIVE_INFINITY});
    primitive_types_and_values
      .put(Long.class, new Object[] {"long", "java.lang.Long",
                                     (long) value,
                                     (long) Double.NaN,
                                     (long) Double.POSITIVE_INFINITY,
                                     (long) Double.NEGATIVE_INFINITY});
    primitive_types_and_values
      .put(Float.class, new Object[] {"float", "java.lang.Float",
                                      (float) value,
                                      (float) Float.NaN,
                                      Float.POSITIVE_INFINITY,
                                      Float.NEGATIVE_INFINITY});
    primitive_types_and_values
      .put(Double.class, new Object[] {"double", "java.lang.Double",
                                       (double) value,
                                       (double) Double.NaN,
                                       Double.POSITIVE_INFINITY,
                                       Double.NEGATIVE_INFINITY});
    for (Entry<Class, Object[]> primitive_infos : 
           primitive_types_and_values.entrySet()) {
      Class    primitive_class = primitive_infos.getKey();
      Object[] names_and_value = primitive_infos.getValue();
      testNumericPrimitive((String) names_and_value[0], value_string,
                           primitive_class, names_and_value[2]);
      testNumericPrimitive((String) names_and_value[1], value_string,
                           primitive_class, names_and_value[2]);
      testNumericPrimitive((String) names_and_value[0], "\"NaN\"",
                           primitive_class, names_and_value[3]);
      testNumericPrimitive((String) names_and_value[1], "\"NaN\"",
                           primitive_class, names_and_value[3]);
      testNumericPrimitive((String) names_and_value[0], "\"Infinity\"",
                           primitive_class, names_and_value[4]);
      testNumericPrimitive((String) names_and_value[1], "\"Infinity\"",
                           primitive_class, names_and_value[4]);
      testNumericPrimitive((String) names_and_value[0], "\"-Infinity\"",
                           primitive_class, names_and_value[5]);
      testNumericPrimitive((String) names_and_value[1], "\"-Infinity\"",
                           primitive_class, names_and_value[5]);
    }


    Object[][] classes_and_literals = 
      new Object[][] {{"java.lang.String",   String.class},
                      {"java.lang.String[]", String[].class},
                      {"int",                int.class},
                      {"java.util.Map",      Map.class}};
    for( Object[] class_and_literal : classes_and_literals) {
      testClassPrimitive((String) class_and_literal[0], 
                         (Class) class_and_literal[1]);
    }

    
  }

  public void testNumericPrimitive(String primitive_name,
                                   String value,
                                   Class expected_class,
                                   Object expected_value) throws Exception {
    String json_string =
      String.format("{category : \"Number\", type : \"%s\", value : %s}",
                    primitive_name,
                    value);
    JSONObject json_structure = new JSONObject(json_string);
    Object parsed_value = parse_numeric_primitive(json_structure);
    assertEquals(expected_class, parsed_value.getClass());
    assertEquals(expected_value, parsed_value);
  }

  public void testClassPrimitive(String classname,
                                 Class expected_class) throws Exception {
    String json_string =
      String.format("{category : \"Object\", type : \"Class\", value : \"%s\"}",
                    classname);
    Class provided_class = parse_class(new JSONObject(json_string));
    assertEquals(expected_class, provided_class);
  }

  public void testStringPrimitive(String string) throws Exception {
    String json_string =
      String.format("{category : \"Object\", type : \"String\", value : \"%s\"}",
                    string);
    String parsed_string = parse_string(new JSONObject(json_string));
    assertEquals(string, parsed_string);
  }

  public void testParseArgument() throws Exception {
    JSONObject string_class_argument =
      new JSONObject("{category : \"Object\", type : \"Class\","+
                     "value : \"java.lang.String\"}");
    assertEquals(String.class, 
                 (Class) parse_argument(string_class_argument));
    JSONObject string_array_class_argument =
      new JSONObject("{category : \"Object\", type : \"Class\","+
                     "value : \"java.lang.String[]\"}");
    assertEquals(String[].class,
                 (Class) parse_argument(string_array_class_argument));
    JSONObject int_class_argument =
      new JSONObject("{category : \"Object\", type : \"Class\","+
                     "value : \"int\"}");
    assertEquals(int.class,
                 (Class) parse_argument(int_class_argument));
    JSONObject int_array_class_argument =
      new JSONObject("{category : \"Object\", type : \"Class\","+
                     "value : \"int[]\"}");
    assertEquals(int[].class,
                 (Class) parse_argument(int_array_class_argument));
    JSONObject string_argument =
      new JSONObject("{category : \"Object\", type : \"String\","+
                     "value : \"Nya\"}");
    assertEquals("Nya", (String) parse_argument(string_argument));
  }

  public void testParseReference() throws Exception {
    String object = "ABC";

    ObjectStorage object_storage = new ObjectLibrary();
    Object object_id = object_storage.store(object);
    String jsoned_argument =
      String.format("{category : \"Object\", type : \"Reference\", value : %s}",
                    object_id.toString());
    JSONObject reference_argument =
      new JSONObject(jsoned_argument);

    assertEquals(object,
                 (String) parse_reference(reference_argument, object_storage));
    
  }

  public void testParseExternalArgument() throws Exception {
    String class_object_string = 
      "{category: \"Object\", type: \"Class\", value: \"java.lang.String\"}";
    JSONObject jsoned_class_object = new JSONObject(class_object_string);
    
    assertEquals(parse_argument(jsoned_class_object),
                 parse_external_argument(class_object_string));
    assertEquals(String.class,
                 (Class) parse_external_argument(class_object_string));

    String array_class_object_string = 
      "{category : \"Object\", type : \"Class\", value : \"int[]\"}";
    JSONObject jsoned_array_class_object = 
      new JSONObject(array_class_object_string);
    assertEquals(parse_argument(jsoned_array_class_object),
                 parse_external_argument(array_class_object_string));
    assertEquals(int[].class,
                 (Class) parse_external_argument(array_class_object_string));

    String number_object_string = 
      "{category : \"Number\", type : \"byte\", value : 87}";
    JSONObject jsoned_number_object = new JSONObject(number_object_string);
    assertEquals(parse_argument(jsoned_number_object),
                 parse_external_argument(number_object_string));
    assertEquals((byte) 87,
                 (byte) ((Byte) parse_external_argument(number_object_string)));

    ObjectLibrary reference_storage = new ObjectLibrary();
    String array_object_string =
      "{category: \"Array\", "+
      "type: \"short\", "+
      "value: [10, -789, {category: \"Object\", type: \"Reference\", value: 0}]}";
    JSONObject jsoned_array_object = new JSONObject(array_object_string);

    short[] represented_array = new short[] {10, -789, 23};

    reference_storage.store(23, (Object) 0);
    short[] parsed_array_external = 
      (short[]) parse_external_argument(array_object_string, reference_storage);
    short[] parsed_array_internal =
      (short[]) parse_argument(jsoned_array_object, reference_storage);

    assertEquals(represented_array.length, parsed_array_external.length);
    assertEquals(represented_array.length, parsed_array_internal.length);

    for (int index = 0; index < represented_array.length; index++) {
      assertEquals(represented_array[index], parsed_array_external[index]);
      assertEquals(represented_array[index], parsed_array_internal[index]);
    }

  }
}
