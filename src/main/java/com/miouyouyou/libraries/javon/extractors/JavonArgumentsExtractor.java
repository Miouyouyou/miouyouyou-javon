package com.miouyouyou.libraries.javon.extractors;

import static com.miouyouyou.libraries.javon.JSONParsers.is_reference;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_argument;
import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_boolean;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_boxed_primitive;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_char;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_floating_point;
import com.miouyouyou.libraries.helpers.PrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.QuickBoxedCaster;
import com.miouyouyou.libraries.json.extractors.JSONExtractor;

import java.lang.reflect.Array;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JavonArgumentsExtractor extends JSONExtractor {

  protected ObjectStorage object_storage;
  protected boolean primitive_component_class;
  protected boolean primitive_number_class;
  protected PrimitiveBoxedCaster primitive_caster;
  protected boolean character_component_class;

  public JavonArgumentsExtractor(Class array_component_class,
                                    ObjectStorage reference_storage) {
    if (array_component_class == null) {
      throw new IllegalArgumentException("Expected a class, got null instead");
    }
    if (reference_storage == null) {
      throw new IllegalArgumentException("Expected an object storage, got "+
                                         "null instead. The object storage is "+
                                         "needed in order to extract the "+
                                         "objects referenced and construct an "+
                                         "Array.");
    }
    this.component_type = array_component_class;
    this.object_storage  = reference_storage;
    this.primitive_component_class = array_component_class.isPrimitive();

    if (!is_boolean(array_component_class) &&
        (this.primitive_component_class ||
         is_boxed_primitive(array_component_class))) {
      this.primitive_number_class = true;
      this.primitive_caster = new QuickBoxedCaster(array_component_class);
      this.character_component_class  = is_char(array_component_class);
    }
    
  }

  @Override
  protected boolean is_good_value(Object value_from_json_array) {
    return true;
  }
  
  @Override
  protected void add_good_value_to(List<Object> list,
                                   Object extracted_value) {
    Object argument_value = parse_argument(extracted_value,
                                           this.object_storage);

    if (argument_value == null) { 
      /* When dealing with primitive component classes (boolean, int, double), 
         adding null to the list will lead to an IllegalArgumentException when
         trying to store the value in the returned array. */
      if (!primitive_component_class) { list.add(null); }
    }
    else if (argument_value != ObjectStorage.no_such_value) {

      if (this.primitive_number_class) {
        if (argument_value instanceof Number) {
          if (is_floating_point(argument_value)) {
            list.add(this.primitive_caster.cast( ((Number) argument_value).
                                                 doubleValue()));
          }
          else {
            list.add(this.primitive_caster.cast( ((Number) argument_value).
                                                 longValue()));
          }
        }
        else if (is_char(argument_value)) {
          list.add(this.primitive_caster.cast((Character) argument_value));
        }
        /* Okay, dealing with 'chars' will require polymorphism to get better
           performances... */
        else if (character_component_class &&
                 argument_value instanceof String &&
                 ((String) argument_value).length() == 1) {
          list.add(((String) argument_value).charAt(0));
        }
      } // end of if(primitive_component_class)

      else if (this.component_type.
               isAssignableFrom(argument_value.getClass())) {
        list.add(argument_value);
      } 
    } 
  }

  @Override
  protected Object create_array(int size) {
    return Array.newInstance(this.component_type, size);
  }

}
