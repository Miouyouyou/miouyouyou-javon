package com.miouyouyou.libraries.javon;

import static com.miouyouyou.libraries.helpers.JavaStringHelpers.list;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_component_class_of;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_primitive_equivalent_of;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_to_literal;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_floating_point;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_primitive;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_special_floating_point;
import static com.miouyouyou.libraries.helpers.JavaExceptionHelpers.get_stacktrace_from;
import static com.miouyouyou.libraries.json.JSONStringHelpers.escape_for_JSON;


import java.lang.reflect.Array;

import java.util.Set;
import gnu.trove.set.hash.THashSet;

public class ResultDisplay {

  public static boolean quoted;
  /* Define the objects representations understood by JSONParsers. */
  public static Set<Class> reparsable_representations;
  static {
    quoted = true;
    reparsable_representations = new THashSet<Class>();
    reparsable_representations.add(Boolean.class);
    reparsable_representations.add(Byte.class);
    reparsable_representations.add(Short.class);
    reparsable_representations.add(Character.class);
    reparsable_representations.add(Integer.class);
    reparsable_representations.add(Long.class);
    reparsable_representations.add(Float.class);
    reparsable_representations.add(Double.class);
    reparsable_representations.add(String.class);
    reparsable_representations.add(Class.class);  
  }

  public static boolean has_javon_representation(Object o) {
    return (o == null ||
            reparsable_representations.contains(o.getClass()) ||
            is_representable_array(o));
  }

  public static boolean is_representable_array(Object o) {
    if (!o.getClass().isArray()) { return false; }
    Class array_component_class = get_component_class_of(o);
    return (is_primitive(array_component_class) ||
            reparsable_representations.contains(array_component_class));
  }

  public static String escape_string(String original_string) {
    return escape_for_JSON(original_string);
  }

  public static String json(String... properties) {
    return String.format("{ %s }", list(",", properties));
  }

  public static String property(String key, String value) {
    return String.format("\"%s\": %s", key, value);
  }

  public static String property(String key, String value, boolean quote_value) {
    return String.format("\"%s\": \"%s\"",
                         key,
                         escape_string(value));
  }

  public static String category(String category) {
    return property("category", category, quoted);
  }

  public static String type(String type) {
    return property("type", type, quoted);
  }

  public static String value(String value) {
    return property("value", value);
  }

  public static String value(String value, boolean is_quoted) {
    return property("value", value, is_quoted);
  }

  public static String to_string(Object o) {
    return property("toString", (o != null ? o.toString() : "null"), quoted);
  }

  public static String get_class(Object o) {
    return property("getClass", class_to_literal(o.getClass()), quoted);
  }

  public static String javon(Object o) {
    if (o == null) { return javon_null_value(); }
    else if (o instanceof Boolean) { return javon_boolean((Boolean) o); }
    else if (o instanceof Number) { return javon_number((Number) o); }
    else if (o instanceof Character) { 
      return javon_character_object((Character) o); 
    }
    else if (o instanceof String) { return javon_string_object((String) o); }
    else if (o instanceof Class) { return javon_class_object((Class) o); }
    else if (o instanceof Exception) { return javon_exception((Exception) o); }
    else if (o.getClass().isArray()) { return javon_array(o); }
    else { return json_other_object(o); }
  }

  public static String javon_null_value() {
    return json(category("Object"), type("null"));
  }

  public static String javon_number(Number number) {
    Class number_class;
    /* Converts boxed primitive classes to primitive classes when possible */
    if ((number_class = get_primitive_equivalent_of(number.getClass())) == null)
      { number_class = number.getClass(); }

    if (is_floating_point(number) && is_special_floating_point(number)) {
      return javon_number(number_class.getName(), 
                             String.format("\"%s\"", number.toString()));
    }
    return javon_number(number_class.getName(), number.toString());
  }

  public static String javon_number(String number_class, String value) {
    return json(category("Number"), type(number_class), value(value));
  }

  public static String javon_string_object(String string) {
    return json(category("Object"), type("String"), value(string, quoted));
  }

  public static String javon_class_object(Class class_object) {
    return json(category("Object"), 
                type("Class"), 
                value(class_to_literal(class_object), quoted));
    
  }

  public static String javon_character_object(Character character) {
    if (Character.isHighSurrogate(character) ||
        Character.isLowSurrogate(character) ||
        Character.isISOControl(character)) {
      /* Is there any way to get the codepoint represented by a 'Character' 
         value without resorting to ugly hacks !? */
      return javon_number("char", 
                          String.format("%d", (int) character.charValue()));
    }
    else {
      return json(category("Object"), type("Character"), 
                  value(character.toString(), quoted));
    }
  }

  public static String javon_boolean(Boolean flag) {
    return json(category("Boolean"),
                value(flag.toString()));
  }

  public static String javon_reference_object(Object id, Object o) {
    return json(category("Object"),
                type("Reference"),
                value(id.toString()),
                to_string(o),
                get_class(o));
  }

  public static String javon_array(Object o) {
    return json(category("Array"),
                type(get_component_class_of(o).getName()),
                value(java_array_to_javon_array(o)));
  }

  public static String java_array_to_javon_array(Object o) {
    if (o == null) { return javon(o); }

    StringBuilder string_builder = new StringBuilder("[");
    Class component_type = o.getClass().getComponentType();
    if (!component_type.isArray()) {
      for (int index = 0; index < Array.getLength(o); index++) {
        string_builder.append(javon(Array.get(o, index)));
        if (index < Array.getLength(o) - 1) {
          string_builder.append(",");
        }
      }
    }
    else {
      for (int index = 0; index < Array.getLength(o); index++) {
        string_builder.append(java_array_to_javon_array(Array.get(o, 
                                                                     index)
                                                           ));
        if (index < Array.getLength(o) - 1) {
          string_builder.append(",");
        }
      }
    }
    string_builder.append("]");
    return string_builder.toString();
  }

  public static String json_other_object(Object o) {
    return json(category("Unknown"),
                to_string(o),
                get_class(o));
  }

  public static String exception_cause(String initial_cause) {
    return property("cause", initial_cause, quoted);
  }

  public static String exception_message(String exception_message) {
    return property("message", exception_message, quoted);
  }

  public static String exception_stacktrace(String exception_stacktrace) {
    return property("stacktrace", exception_stacktrace, quoted);
  }

  public static String javon_exception(Exception exception) {
    String initial_cause, message, stacktrace;
    if (exception.getCause() != null) {
      initial_cause = exception.getCause().getClass().getName();
    }
    else { initial_cause = "thrown"; }
    if (exception.getMessage() != null) {
      message = exception.getMessage();
    } 
    else { message = ""; }

    try {
      stacktrace = get_stacktrace_from(exception);
    } 
    catch (RuntimeException e) {
      String runtime_original_cause = (e.getCause() != null ?
                                       e.getCause().getClass().getName() :
                                       "RuntimeException");
        
      stacktrace = 
        String.format("Encountered an %s while trying to print the "+
                      "stacktrace. Message was : %s",
                      runtime_original_cause,
                      e.getMessage());
    }

    return json(category("Exception"),
                get_class(exception),
                exception_cause(initial_cause),
                exception_message(message),
                exception_stacktrace(stacktrace),
                to_string(exception));
  }
}
