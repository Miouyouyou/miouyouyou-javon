package com.miouyouyou.libraries.javon.exceptions;

import java.lang.RuntimeException;

public class MalformedCall extends RuntimeException {
  public MalformedCall() {}
  public MalformedCall(String message) { super(message); }
  public MalformedCall(Throwable cause) { super(cause); }
  public MalformedCall(String message, Throwable cause) {
    super(message, cause);
  }
}
