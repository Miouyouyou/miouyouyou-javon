package com.miouyouyou.libraries.javon.interfaces;

public interface ObjectStorage {
  public enum Have { NO_SUCH_VALUE };
  public Object no_such_value = ObjectStorage.Have.NO_SUCH_VALUE;
  public Object store(Object object, Object id);
  public Object store(Object object);
  public Object retrieve(Object id);
}
