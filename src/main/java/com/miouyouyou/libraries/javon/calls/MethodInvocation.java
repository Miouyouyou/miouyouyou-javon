package com.miouyouyou.libraries.javon.calls;

import com.miouyouyou.libraries.javon.JSONCall;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_argument;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_arguments;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_signature;
import com.miouyouyou.libraries.javon.exceptions.MalformedCall;
import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.get_boxed_equivalent_of;

import org.json.JSONObject;
import org.json.JSONException;

import java.lang.reflect.Method;

public class MethodInvocation implements JSONCall {

  private Object receiver;
  private Method method_to_invoke;
  private Object[] arguments;

  public MethodInvocation(String json_string,
                          ObjectStorage reference_storage) {
    final JSONObject json_call = check(json_string);
    Object provided_receiver = null;

    try {
      provided_receiver = parse_argument(json_call.get("receiver"),
                                         reference_storage);
    } 
    catch (JSONException e) { throw new MalformedCall(e); }

    parse_method_from(json_call, reference_storage);
    this.arguments = parse_arguments(json_call, reference_storage);
  }

  public static JSONObject check(String json_string) {
    JSONObject checked_json_call = null;
    try {
      checked_json_call = new JSONObject(json_string);
    } 
    catch (JSONException e) { throw new MalformedCall(e); }
    if (checked_json_call.isNull("name") ||
        checked_json_call.isNull("receiver")) {
      String error = 
        String.format("In order to invoke a method, I need, at least, :\n"+
                      "- its name defined by a key \"name\";"+
                      "- the object providing the named method, defined in "+
                      "a key named \"receiver\".\n"+
                      "No null value should be associated with these keys."+
                      "The parsed JSONString was : %s", 
                      checked_json_call.toString());
      throw new MalformedCall(error);
    }
    return checked_json_call;
  }

  public static void check_receiver_class(Class receiver_class) {
    if (receiver_class.isArray()) {
      throw new MalformedCall("You cannot call methods directly on Array "+
                              "through Reflection.\n"+
                              "Use java.lang.reflect.Array static methods "+
                              "instead.");
    }
  }

  public void parse_method_from(JSONObject jsoned_call,
                                ObjectStorage reference_storage) {
    try {
      parse_method_from(parse_argument(jsoned_call.get("receiver"),
                                       reference_storage),
                        jsoned_call.getString("name"),
                        parse_signature(jsoned_call));
    } 
    catch (JSONException e) { throw new MalformedCall(e); }
  }

  public void parse_method_from(Object receiver, String method_name,
                                Class[] method_parameters) {
    if (receiver instanceof Class) {
      parse_method_from(null, (Class) receiver, method_name,
                        method_parameters);
    } 
    else { 
      parse_method_from(receiver, receiver.getClass(), method_name,
                        method_parameters); }
  }

  public void parse_method_from(Object receiver,
                                Class receiver_class,
                                String method_name,
                                Class[] method_parameters) {
    check_receiver_class(receiver_class);
    Method desired_method = null;
    try {
      desired_method = 
        receiver_class.getMethod(method_name, method_parameters);
    } 
    catch (NoSuchMethodException e) { throw new MalformedCall(e); }
    this.receiver = receiver;
    this.method_to_invoke = desired_method;
  }

  public Object execute() {
    try {
      return this.method_to_invoke.invoke(this.receiver,
                                          this.arguments);
    }
    catch (Exception e) { throw new MalformedCall(e); }
  }
}
