package com.miouyouyou.libraries.javon.calls;

import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_from_literal;
import com.miouyouyou.libraries.javon.JSONCall;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_signature;
import static com.miouyouyou.libraries.javon.JSONParsers.parse_arguments;
import com.miouyouyou.libraries.javon.exceptions.MalformedCall;
import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

public class NewObject implements JSONCall {

  final private Constructor instantiator;
  final private Object[] arguments;

  public NewObject(String json_call_string, ObjectStorage reference_storage) {
    final JSONObject constructor_informations = validate(json_call_string);   
    final Class object_class = get_class_from(constructor_informations);   
    final Class[] signature = parse_signature(constructor_informations);
    

    try { this.instantiator = object_class.getConstructor(signature); }
    catch (NoSuchMethodException e) { 
      throw new 
        MalformedCall("No constructor matching this signature was found", e);
    }
    this.arguments = parse_arguments(constructor_informations, reference_storage);

    /*final Class[] signature = 
      parseSignature(constructor_informations.get("signature"));
    final Object[] arguments =
      parseArguments(constructor_informations.get("arguments"));
    Constructor instantiator;*/
    
  }

  private Class get_class_from(JSONObject json_call) {
    String receiver = null; 
    
    try {
      receiver = json_call.getString("receiver");
      return class_from_literal(receiver);
    } 
    catch (JSONException e) {
      /* validate(String) already made sure that :
         - the key "receiver" is available;
         - the value associated to "receiver" is a String.
         So there's no clear reason for a JSONException to be thrown here... */
      throw new MalformedCall(e);
    } 
    catch (ClassNotFoundException e) {
      String error = 
        String.format("The class %s was not found. \n"+
                      "Be sure to provide a fully qualified literal. E.g. "+
                      "java.lang.String for String, java.lang.String[] for "+
                      "String[] or int for int.", receiver);
      throw new MalformedCall(error, e);
    }
  }

  public Object execute() {
    try {
      return this.instantiator.newInstance(this.arguments);
    } 
    catch (IllegalAccessException e)    { throw new RuntimeException(e); }
    catch (InstantiationException e)    { throw new RuntimeException(e); }
    catch (InvocationTargetException e) { throw new RuntimeException(e); }    
  }

  private JSONObject validate(String json_call_string) {
    JSONObject call;
    try {
      call = new JSONObject(json_call_string);
    } catch (JSONException e) {
      String error = String.format("Malformed JSON String : %s",
                                   json_call_string);
      throw new MalformedCall(error, e);
    }    
    if (!call.has("receiver") ||
        call.isNull("receiver")) {
      throw new MalformedCall("No receiver specified");
    }
    
    return call;
  }
  

}
