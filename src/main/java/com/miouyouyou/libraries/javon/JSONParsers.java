package com.miouyouyou.libraries.javon;

import com.miouyouyou.libraries.javon.calls.NewObject;
import com.miouyouyou.libraries.javon.calls.MethodInvocation;
import com.miouyouyou.libraries.javon.exceptions.MalformedCall;
import com.miouyouyou.libraries.javon.extractors.JavonArgumentsExtractor;
import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;

import com.miouyouyou.libraries.json.JSONArrayToArray;
import static com.miouyouyou.libraries.json.JSONNestedArraysToArray.to_array;

import com.miouyouyou.libraries.helpers.PrimitiveBoxedCaster;
import com.miouyouyou.libraries.helpers.QuickBoxedCaster;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_from_literal;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.class_to_literal;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_boxed_primitive;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_char;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_float;
import static com.miouyouyou.libraries.helpers.JavaClassHelpers.is_floating_point;

import gnu.trove.map.hash.THashMap;

import java.lang.reflect.Array;
import java.util.Map;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

public class JSONParsers {

  private static Map<Class,Map> special_numbers;

  static {
    Map<String,Double> double_map = new THashMap<String,Double>();
    double_map.put("NaN",       Double.NaN);
    double_map.put("Infinity",  Double.POSITIVE_INFINITY);
    double_map.put("-Infinity", Double.NEGATIVE_INFINITY);
    Map<String,Float> float_map = new THashMap<String,Float>();
    float_map.put("NaN",       Float.NaN);
    float_map.put("Infinity",  Float.POSITIVE_INFINITY);
    float_map.put("-Infinity", Float.NEGATIVE_INFINITY);

    special_numbers = new THashMap<Class,Map>();
    special_numbers.put(Double.class, double_map);
    special_numbers.put(double.class, double_map);
    special_numbers.put(Float.class, float_map);
    special_numbers.put(float.class, float_map);
  }

  public static boolean has_appropriate_signature(JSONObject json_call) {
    try {
      if (!json_call.has("signature") ||
          !(json_call.get("signature") instanceof JSONArray)) {
        return false;
      } else {
        return true;
      }
    } 
    catch (JSONException e) { return false; }
  }

  public static void check_argument_structure(JSONObject json_call) {
    try {
      if (json_call == null) { 
        throw new MalformedCall("Expected a JSON object, got null instead");
      }
      if (!json_call.has("category") ||
          !json_call.has("type")     ||
          !json_call.has("value")) {
        String error = 
          String.format("Expected a category, a type and a value key in %s",
                        json_call.toString());
        throw new MalformedCall(error);
      }

      if (json_call.get("category").getClass() != String.class) {
        String error = 
          String.format("The value associated with the category key should be "+
                        "a String. (Parsed JSON was %s)", 
                        json_call.toString());
        throw new MalformedCall(error);
      }
    } catch (JSONException e) { throw new MalformedCall(e); }
  }

  public static boolean is_a_jsoned_call(JSONObject jsoned_argument) {
    return !(jsoned_argument.isNull("call"));
  }

  public static String calltype(JSONObject jsoned_argument) 
    throws JSONException {
    if (!is_a_jsoned_call(jsoned_argument)) return null;
    return jsoned_argument.getString("call").intern();
  }

  public static String type_of(JSONObject jsoned_argument) 
    throws JSONException {
    if (!jsoned_argument.has("type")) return null;
    return jsoned_argument.getString("type").intern();
  }

  public static String category_of(JSONObject jsoned_argument) 
    throws JSONException {
    if (!jsoned_argument.has("category")) return null;
    return jsoned_argument.getString("category").intern();
  }

  public static Object value_of(JSONObject jsoned_argument) 
    throws JSONException {
    if (!jsoned_argument.has("value") ||
        jsoned_argument.isNull("value")) return null;
    return jsoned_argument.get("value");
  }

  public static String string_value_of(JSONObject jsoned_argument) 
    throws JSONException {
    return jsoned_argument.getString("value");
  }

  public static Boolean boolean_value_of(JSONObject jsoned_argument)
    throws JSONException {
    if (jsoned_argument.isNull("value")) { return null; }
    return jsoned_argument.getBoolean("value");
  }

  public static boolean value_is_null_in(JSONObject jsoned_argument) {
    return jsoned_argument.isNull("value");
  }

  public static boolean is_reference(JSONObject jsoned_argument) {
    try {
      jsoned_argument.getLong("value");
      return (category_of(jsoned_argument) == "Reference");
    } catch (JSONException e) { return false; }
  }

  public static boolean is_valid_reference_id(Object extracted_value) {
    return (extracted_value instanceof Long);
  }

  public static Object parse_call(JSONObject jsoned_argument) {
    return parse_call(jsoned_argument, null);
  }

  public static Object parse_call(JSONObject jsoned_call,
                                  ObjectStorage reference_storage) {
    try {
      String call_type = calltype(jsoned_call);
      if (call_type == "new") {
        return new NewObject(jsoned_call.toString(), reference_storage).execute();
      }
      else if (call_type == "invoke_method") {
        return new MethodInvocation(jsoned_call.toString(), reference_storage).execute();
      }
      else {
        String error = String.format("Unknown call type : %s", call_type);
        throw new MalformedCall(error);
      }
    } catch (JSONException e) { throw new MalformedCall(e); }
  }

  public static Object parse_external_argument(String json_string) {
    return parse_external_argument(json_string, null);
  }

  public static Object parse_external_argument(String json_string,
                                               ObjectStorage reference_storage)
  {
    try {
      return parse_argument(new JSONObject(json_string), 
                            reference_storage);
    }
    catch (JSONException e) { throw new MalformedCall(e); }
  }

  public static Object parse_argument(Object o) {
    return parse_argument(o, null);
  }

  public static Object parse_argument(Object o,
                                      ObjectStorage reference_storage) {
    if (o instanceof JSONObject) {
      return parse_argument((JSONObject) o, reference_storage);
    } else { return o; }
  }

  public static Object parse_argument(JSONObject jsoned_argument) {
    return parse_argument(jsoned_argument, null);
  }


  public static Object parse_argument(JSONObject jsoned_argument,
                                      ObjectStorage reference_storage) {
    try {
      if (is_a_jsoned_call(jsoned_argument)) {
        return parse_call(jsoned_argument, reference_storage);
      }
      String category = category_of(jsoned_argument);
      String type     = type_of(jsoned_argument);

      if (category == "Number") {
        return parse_numeric_primitive(type,
                                       value_of(jsoned_argument));
      }
      else if (category == "Boolean") {
        return boolean_value_of(jsoned_argument);
      }
      else if (category == "Object") {
        return parse_object(type, jsoned_argument, reference_storage);
      } 
      else if (category == "Array") {
        return parse_array(jsoned_argument, reference_storage);
      }
      else {
        String error_message = 
          String.format("Category %s unknown. Parsed JSON was %s",
                        category,
                        jsoned_argument.toString());
        throw new MalformedCall(error_message);
      }
    } 
    catch (JSONException e) { throw new MalformedCall(e); }
  }

  public static Object[] parse_arguments(JSONObject jsoned_argument) {
    return parse_arguments(jsoned_argument, null);
  }

  public static Object[] parse_arguments(JSONObject jsoned_argument,
                                         ObjectStorage reference_storage) {
    JSONArray arguments = jsoned_argument.optJSONArray("arguments");
    if (arguments == null) { return null; }
    return parse_arguments(arguments, reference_storage);
  }

  public static Object[] parse_arguments(JSONArray arguments,
                                         ObjectStorage reference_storage) {
    return (Object[]) parse_simple_array(Object.class,
                                         arguments,
                                         reference_storage);
  }

  public static Object parse_object(JSONObject jsoned_argument) 
    throws JSONException {
    return parse_object(jsoned_argument, (ObjectStorage) null);
  }

  public static Object parse_object(JSONObject jsoned_argument,
                                    ObjectStorage reference_storage) 
    throws JSONException {
    return parse_object(type_of(jsoned_argument), 
                        jsoned_argument, 
                        reference_storage);
  }

  public static Object parse_object(String type,
                                    JSONObject jsoned_argument,
                                    ObjectStorage reference_storage) 
    throws JSONException {
    if (type == null   ||
        type == "null" ||
        value_is_null_in(jsoned_argument)) { return null; }
    else if (type == "String") { return string_value_of(jsoned_argument); }
    else if (type == "Character") {
      return parse_character(value_of(jsoned_argument));
    }
    else if (type == "Class") {
      return parse_class(string_value_of(jsoned_argument));
    }
    else if (type == "Reference") {
      return parse_reference(reference_storage,
                             jsoned_argument.getLong("value"));
    }
    else { return string_value_of(jsoned_argument); }

  }

  public static Object parse_character(JSONObject jsoned_argument) 
    throws JSONException {
    return parse_character(value_of(jsoned_argument));
  }

  public static Character parse_character(Object value) {
    if (value instanceof Number) {
      return (Character) ((char) ((Number) value).intValue());
    } 
    else { return value.toString().charAt(0); }
  }

  public static Object parse_numeric_primitive(JSONObject jsoned_argument) 
    throws JSONException {
    return parse_numeric_primitive(type_of(jsoned_argument), 
                                   value_of(jsoned_argument));
  }

  public static Object parse_numeric_primitive(String type_name, Object value) {

    Class literal_class = null;
    /* This will help cast any primitive type to any boxed primitive type.
       Example : Long to Byte or int to Character... */
    PrimitiveBoxedCaster caster = null;

    /* IllegalArgumentException is thrown by QuickBoxedCaster if the class 
       provided isn't a number primitive or boxed number primitive (e.g. int
       or Integer). The exception's message is self explanatory. */
    try {
      literal_class = class_from_literal(type_name);
      caster        = new QuickBoxedCaster(literal_class);
    } 
    catch (ClassNotFoundException e)   { throw new MalformedCall(e); } 
    catch (IllegalArgumentException e) { throw new MalformedCall(e); }

 
    /* PrimitiveBoxedCaster#cast() have two signatures, 'long' and 'double'.
       JSONObject#get(key) provide an Integer or Long, for integer numbers and
       a Double object for floating-point values.
       The problem is : Since I don't know the class of the object returned by
       JSONObject#get in advance, I stored it in an Object-typed variable. 
            
       Forcing 'double', by storing the value in a Double-typed variable and
       using JSONObject#getDouble(key) for everything might denormalize legit 
       long values (e.g. values between 2^53 and 2^63-1). On the other hand, 
       forcing 'long' will truncate legit floating-point values... */

    if (value instanceof String) {
      String value_string = ((String) value).intern();
      if (value_string.length() == 1) {
        return caster.cast(((String) value).charAt(0));
      }

      else {
        boolean floating_point_literal_class = 
          is_floating_point(literal_class);
        Class special_number_class = 
          (floating_point_literal_class ? literal_class : Double.class);
        Object special_number = 
          special_numbers.get(special_number_class).get(value_string);
        if (special_number != null) {
          if (floating_point_literal_class) { return special_number; }
          else {
            return caster.cast((double) ((Double) special_number));
          }
        }
        else { // That will happen if value_string is just a quoted number
          if (is_floating_point(literal_class)) {
            return caster.cast(Double.parseDouble(value_string));
          } 
          else { return caster.cast(Long.parseLong(value_string)); }
        }
      }

    }
    else if (value instanceof Double) {
      return caster.cast(((Number) value).doubleValue());
    }
    else { return caster.cast(((Number) value).longValue()); }
  }

  public static String parse_string(JSONObject jsoned_argument) 
    throws JSONException {
    return jsoned_argument.getString("value");
  }

  public static Class parse_class(JSONObject jsoned_argument) 
    throws JSONException {
    return parse_class(string_value_of(jsoned_argument));
  }

  public static Class parse_class(String class_literal) {
    try {
      Class parsed_class = class_from_literal(class_literal);
      return parsed_class;
    } catch (ClassNotFoundException e) { 
      String error = 
        String.format("The class %s was not found.", class_literal);
      throw new MalformedCall(error); 
    }
  }

  public static Class[] parse_signature(JSONObject json_call) {
    if (!has_appropriate_signature(json_call)) return null;

    try {
      // Named 'literals' because the expected strings must be literal 
      // transcription of the desired classes, like 'int' or 
      // 'java.lang.String[]'. 'Names' would be a bit different. E.g. : 
      // the name of 'java.lang.String[]' is actually '[Ljava.lang.String;'
      String[] literals = 
        (String[]) new JSONArrayToArray(String.class).
                   extract_values(json_call.getJSONArray("signature"));

      if (literals.length == 0) return null;

      Class[] classes = new Class[literals.length];

      try {
        for (int index = 0; index < classes.length; index++) {
          classes[index] = class_from_literal(literals[index]);
        }
        return classes;
      } catch (ClassNotFoundException e) { throw new MalformedCall(e); }

    } catch (JSONException e) { throw new MalformedCall(e); }
  }

  public static Object parse_reference(JSONObject jsoned_argument,
                                       ObjectStorage object_storage) 
    throws JSONException {
    if (!jsoned_argument.has("value"))
      { return ObjectStorage.no_such_value; }
    return parse_reference(object_storage,
                           jsoned_argument.get("value"));
  }

  public static Object parse_reference(ObjectStorage object_storage,
                                       Object id) {
    return object_storage.retrieve(id);
  }

  public static Object parse_array(JSONObject jsoned_argument,
                                   ObjectStorage reference_storage) {
    try {
      return parse_array(class_from_literal(type_of(jsoned_argument)),
                         jsoned_argument.getJSONArray("value"),
                         reference_storage);
    } 
    catch (ClassNotFoundException e) { throw new MalformedCall(e); }
    catch (JSONException e) { 
      throw new MalformedCall("Expected an Array as value."+
                              "(e.g -> value : [..., ...])", e);
    } 
  }

  public static Object parse_array(Class component_class,
                                   JSONArray jsoned_array) {
    return parse_array(component_class, jsoned_array, null);
  }

  public static Object parse_array(Class component_class,
                                   JSONArray jsoned_array,
                                   ObjectStorage reference_storage) {

    if (component_class == null) {
      throw new MalformedCall("Expected a component class for the array, "+
                              "got null.");
    }

    /* This need to be clarified with a boolean method implemented in the
       object providing to_array... */ 
    return to_array(jsoned_array,
                    new JavonArgumentsExtractor(component_class,
                                                reference_storage));

  }

  public static Object parse_simple_array(Class component_class,
                                          JSONArray jsoned_array,
                                          ObjectStorage reference_storage) {
    return (new JavonArgumentsExtractor(component_class,
                                           reference_storage).
            extract_values(jsoned_array));
  }



}
