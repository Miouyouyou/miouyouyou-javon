package com.miouyouyou.libraries.javon;

import com.miouyouyou.libraries.javon.interfaces.ObjectStorage;

import gnu.trove.map.hash.TLongObjectHashMap;

public class ObjectLibrary implements ObjectStorage {

  final private TLongObjectHashMap<Object> storage;

  private volatile long next_id = 0;

  public ObjectLibrary() {
    storage = new TLongObjectHashMap<Object>();
  }

  public synchronized Object store(Object object, Object id) {
    if (is_appropriate_id(id)) {
      return store(object, ((Number) id).longValue());
    } else { return null; }
  }

  public synchronized long store(Object object, long id) {
    storage.put(id, object);
    next_id = (id > next_id ? id : next_id);
    return id;
  }

  public synchronized Object store(Object object) {
    return store(object, getAvailableId());
  }

  public synchronized Object retrieve(Object id) {
    if (is_appropriate_id(id)) { 
      return retrieve(((Number) id).longValue());
    } else { return no_such_value; }
  }

  public synchronized Object retrieve(long id) {
    if (storage.containsKey(id)) { return storage.get(id); }
    else return no_such_value;
  }

  private synchronized boolean is_appropriate_id(Object id) {
    return (id != null && (id instanceof Number));
  }

  private synchronized long getAvailableId() {
    long id = next_id;
    for (; storage.containsKey(id); id++);
    next_id = id;
    return next_id;
  }

}
